
public class BandCharging {

	public BandCharging() {

	}

	public String bands(String name) {
		switch (name) {
		case "Periphery":
			return "https://www.facebook.com/PeripheryBand/";
		case "Architects":
			return "https://www.facebook.com/architectsuk/";
		case "After the Burial":
			return "https://www.facebook.com/aftertheburial/";
		case "Animals as Leaders":
			return "https://www.facebook.com/animalsasleaders/";
		case "Insomnium":
			return "https://www.facebook.com/insomniumofficial/";
		case "Brojob":
			return "https://www.facebook.com/BROJOBOFFICIAL";
		case "SallyAnne":
			return "https://www.facebook.com/SallyAnneOfficial";
		case "Black Sequoia":
			return "https://www.facebook.com/blacksequoiaofficial/";
		case "Omnium Gaetherum":
			return "https://www.facebook.com/omniumgatherumband/";
		default:
			return "No facebook page for this band";
		}
	}

}
