﻿// <copyright file="BaseDto.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <summary>This is the BaseDto.</summary>
namespace MusiciansAndBands.Dtos
{
    /// <summary>
    /// Base object for other data transfer objects
    /// </summary>
    public abstract class BaseDto
    {
    }
}
