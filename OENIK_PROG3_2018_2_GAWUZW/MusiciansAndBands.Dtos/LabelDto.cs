﻿// <copyright file="LabelDto.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <summary>This is the LabelDto.</summary>
namespace MusiciansAndBands.Dtos
{
    using System;

    /// <summary>
    /// Label data transfer object
    /// </summary>
    public class LabelDto : BaseDto
    {
        /// <summary>
        /// Gets or sets label_name
        /// </summary>
        /// <value>
        /// Label_name
        /// </value>
        public string Label_name { get; set; }

        /// <summary>
        /// Gets or sets form_date
        /// </summary>
        /// <value>
        /// Form_date
        /// </value>
        public DateTime Form_date { get; set; }

        /// <summary>
        /// ToString method
        /// </summary>
        /// <returns>string values</returns>
        public override string ToString()
        {
            return Label_name + "\n" + Form_date.ToLongDateString();
        }
    }
}
