﻿// <copyright file="BandDto.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <summary>This is the BandDto.</summary>
namespace MusiciansAndBands.Dtos
{
    using System;

    /// <summary>
    /// Band data transfer object
    /// </summary>
    public class BandDto : BaseDto
    {
        /// <summary>
        /// Gets or sets band_ID
        /// </summary>
        /// <value>
        /// Band_ID
        /// </value>
        public int Band_ID { get; set; }

        /// <summary>
        /// Gets or sets band_name
        /// </summary>
        /// <value>
        /// Band_name
        /// </value>
        public string Band_name { get; set; }

        /// <summary>
        /// Gets or sets form_date
        /// </summary>
        /// <value>
        /// Form_date
        /// </value>
        public DateTime Form_date { get; set; }

        /// <summary>
        /// Gets or sets label
        /// </summary>
        /// <value>
        /// Label
        /// </value>
        public string Label { get; set; }

        /// <summary>
        /// Gets or sets hometown
        /// </summary>
        /// <value>
        /// Hometown
        /// </value>
        public string Hometown { get; set; }

        /// <summary>
        /// ToString method
        /// </summary>
        /// <returns>string values</returns>
        public override string ToString()
        {
            return Band_name + "\n" + Form_date.ToLongDateString() + "\n" + Label + "\n" + Hometown;
        }
    }
}
