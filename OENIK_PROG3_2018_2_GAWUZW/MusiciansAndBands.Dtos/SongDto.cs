﻿// <copyright file="SongDto.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <summary>This is the SongDto.</summary>
namespace MusiciansAndBands.Dtos
{
    /// <summary>
    /// Song data transfer object
    /// </summary>
    public class SongDto : BaseDto
    {
        /// <summary>
        /// Gets or sets song_ID
        /// </summary>
        /// <value>
        /// Song_ID
        /// </value>
        public int Song_ID { get; set; }

        /// <summary>
        /// Gets or sets band
        /// </summary>
        /// <value>
        /// Band
        /// </value>
        public string Band { get; set; }

        /// <summary>
        /// Gets or sets band_ID
        /// </summary>
        /// <value>
        /// Band_ID
        /// </value>
        public int Band_ID { get; set; }

        /// <summary>
        /// Gets or sets title
        /// </summary>
        /// <value>
        /// Title
        /// </value>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets album
        /// </summary>
        /// <value>
        /// Album
        /// </value>
        public string Album { get; set; }

        /// <summary>
        /// Gets or sets album_ID
        /// </summary>
        /// <value>
        /// Album_ID
        /// </value>
        public int Album_ID { get; set; }

        /// <summary>
        /// Gets or sets genre
        /// </summary>
        /// <value>
        /// Genre
        /// </value>
        public string Genre { get; set; }

        /// <summary>
        /// ToString method
        /// </summary>
        /// <returns>string values</returns>
        public override string ToString()
        {
            return Song_ID + "\n" + Band + "\n" + Title + "\n" + Album + "\n" + Genre;
        }
    }
}
