﻿// <copyright file="MusicianDto.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <summary>This is the MusicianDto.</summary>
namespace MusiciansAndBands.Dtos
{
    using System;

    /// <summary>
    /// Musician data transfer object
    /// </summary>
    public class MusicianDto : BaseDto
    {
        /// <summary>
        /// Gets or sets musician_name
        /// </summary>
        /// <value>
        /// Musician_name
        /// </value>
        public string Musician_name { get; set; }

        /// <summary>
        /// Gets or sets date_of_birth
        /// </summary>
        /// <value>
        /// Date_of_birth
        /// </value>
        public DateTime Date_of_birth { get; set; }

        /// <summary>
        /// Gets or sets hometown
        /// </summary>
        /// <value>
        /// Hometown
        /// </value>
        public string Hometown { get; set; }

        /// <summary>
        /// ToString method
        /// </summary>
        /// <returns>string values</returns>
        public override string ToString()
        {
            return Musician_name + "\n" + Date_of_birth.ToLongDateString() + "\n" + Hometown;
        }
    }
}
