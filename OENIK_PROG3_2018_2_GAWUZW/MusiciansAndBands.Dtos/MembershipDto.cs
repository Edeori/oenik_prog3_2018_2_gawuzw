﻿// <copyright file="MembershipDto.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <summary>This is the MembershipDto.</summary>
namespace MusiciansAndBands.Dtos
{
    /// <summary>
    /// Membership data transfer object
    /// </summary>
    public class MembershipDto : BaseDto
    {
        /// <summary>
        /// Gets or sets membership_ID
        /// </summary>
        /// <value>
        /// Membership_ID
        /// </value>
        public int Membership_ID { get; set; }

        /// <summary>
        /// Gets or sets position
        /// </summary>
        /// <value>
        /// Position
        /// </value>
        public string Position { get; set; }

        /// <summary>
        /// Gets or sets musician_name
        /// </summary>
        /// <value>
        /// Musician_name
        /// </value>
        public string Musician_name { get; set; }

        /// <summary>
        /// Gets or sets date_of_birth
        /// </summary>
        /// <value>
        /// Date_of_birth
        /// </value>
        public System.DateTime Date_of_birth { get; set; }

        /// <summary>
        /// Gets or sets band
        /// </summary>
        /// <value>
        /// Band
        /// </value>
        public string Band { get; set; }

        /// <summary>
        /// Gets or sets band_ID
        /// </summary>
        /// <value>
        /// Band_ID
        /// </value>
        public int Band_ID { get; set; }
    }
}
