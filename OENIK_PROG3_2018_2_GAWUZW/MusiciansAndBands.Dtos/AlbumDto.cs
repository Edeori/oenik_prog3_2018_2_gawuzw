﻿// <copyright file="AlbumDto.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <summary>This is the AlbumDto.</summary>
namespace MusiciansAndBands.Dtos
{
    using System;

    /// <summary>
    /// Album data transfer object
    /// </summary>
    public class AlbumDto : BaseDto
    {
        /// <summary>
        /// Gets or sets album_ID
        /// </summary>
        /// <value>
        /// Album_ID
        /// </value>
        public int Album_ID { get; set; }

        /// <summary>
        /// Gets or sets album_name
        /// </summary>
        /// <value>
        /// Album_name
        /// </value>
        public string Album_name { get; set; }

        /// <summary>
        /// Gets or sets year_Released
        /// </summary>
        /// <value>
        /// Year_Released
        /// </value>
        public DateTime Year_Released { get; set; }

        /// <summary>
        /// Gets or sets label
        /// </summary>
        /// <value>
        /// Label
        /// </value>
        public string Label { get; set; }

        /// <summary>
        /// Gets or sets producer
        /// </summary>
        /// <value>
        /// Producer
        /// </value>
        public string Producer { get; set; }

        /// <summary>
        /// Gets or sets bandName
        /// </summary>
        /// <value>
        /// BandName
        /// </value>
        public string BandName { get; set; }

        /// <summary>
        /// Gets or sets bandId
        /// </summary>
        /// <value>
        /// BandId
        /// </value>
        public string BandId { get; set; }

        /// <summary>
        /// ToString method
        /// </summary>
        /// <returns>string values</returns>
        public override string ToString()
        {
            return Album_name + "\n" + Year_Released.ToLongDateString() + "\n" + Label + "\n" + Producer + "\n" + BandName;
        }
    }
}
