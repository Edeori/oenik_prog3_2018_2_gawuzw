﻿// <copyright file="AlbumException.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <summary>This is the MusiciansAndBands.Shared.</summary>
namespace MusiciansAndBands.Shared
{
    using System;

    /// <summary>
    /// Custom Exception for albums
    /// </summary>
    [Serializable]
    public class AlbumException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AlbumException"/> class.
        /// </summary>
        /// <param name="message">Message to throw</param>
        public AlbumException(string message)
            : base(message)
        {
        }
    }
}
