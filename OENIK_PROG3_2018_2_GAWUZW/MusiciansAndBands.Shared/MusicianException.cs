﻿// <copyright file="MusicianException.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <summary>This is a Shared object.</summary>
namespace MusiciansAndBands.Shared
{
    using System;

    /// <summary>
    /// Custom Exception for musicians
    /// </summary>
    [Serializable]
    public class MusicianException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MusicianException"/> class.
        /// </summary>
        /// <param name="message">Message to throw</param>
        public MusicianException(string message)
            : base(message)
        {
        }
    }
}
