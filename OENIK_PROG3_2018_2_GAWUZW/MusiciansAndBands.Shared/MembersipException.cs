﻿// <copyright file="MembersipException.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <summary>This is a Shared object.</summary>
namespace MusiciansAndBands.Shared
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Custom Exception for memberships
    /// </summary>
    [Serializable]
    public class MembersipException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MembersipException"/> class.
        /// </summary>
        /// <param name="message">Message to throw</param>
        public MembersipException(string message)
            : base(message)
        {
        }
    }
}
