﻿// <copyright file="StringsToConsole.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <summary>This is a Shared object.</summary>
namespace MusiciansAndBands.Shared
{
    using System;

    /// <summary>
    /// Strings used in the application
    /// </summary>
    public static class StringsToConsole
    {
        /// <summary>
        /// Standard menu text
        /// </summary>
        public static readonly string MenuText = "*********************************\n" +
                                   "* Available Tables:             *\n" +
                                   "*********************************\n" +
                                   "* Bands                         *\n" +
                                   "* Labels                        *\n" +
                                   "* Albums                        *\n" +
                                   "* Musicians                     *\n" +
                                   "* Songs                         *\n" +
                                   "*********************************\n";

        /// <summary>
        /// Standard menu keywords
        /// </summary>
        public static readonly string MenuKeyWords = "*************************************\n" +
                                       "* Available Keywords:               *\n" +
                                       "*************************************\n" +
                                       "* ListAll.{Table}                   *\n" +
                                       "* Get.{ID}.{Table}                  *\n" +
                                       "* Create.{Table}                    *\n" +
                                       "* Update.{ID}.{Table}               *\n" +
                                       "* Delete.{ID}.{Table}               *\n" +
                                       "* Exit                              *\n" +
                                       "*************************************\n" +
                                       "* Facebook         Get band FB Page *\n" +
                                       "*************************************\n" +
                                       "* Add .Multiple keyword after       *\n" +
                                       "* create method call if you         *\n" +
                                       "* want to add multiple.             *\n" +
                                       "* (example: Create.Albums.Multiple) *\n" +
                                       "*************************************\n" +
                                       "* Exit - for stop adding            *\n" +
                                       "*************************************\n";

        /// <summary>
        /// Special menu text
        /// </summary>
        public static readonly string SpecialMenu = "*************************************\n" +
                                           "* Start with 'spec.' for using the  *\n" +
                                           "* following menu items!             *\n" +
                                           "* (example: spec.SongsByAlbum)      *\n" +
                                           "*************************************\n" +
                                           "* SongsByAlbum                      *\n" +
                                           "* SongsByBand                       *\n" +
                                           "* BandsByGenre                      *\n" +
                                           "* MusiciansByBand                   *\n" +
                                           "* MusiciansOnAlbum                  *\n" +
                                           "* MusiciansUnderLabel               *\n" +
                                           "*************************************\n";

        /*/
         *  BAND
        /*/

        /// <summary>
        /// CreateBand
        /// </summary>
        public static readonly string CreateBand = "Create Band";

        /// <summary>
        /// CreateMultipleBands
        /// </summary>
        public static readonly string CreateMultipleBands = "Create Multiple Bands";

        /// <summary>
        /// UpdateBand
        /// </summary>
        public static readonly string UpdateBand = "Update Band";

        /// <summary>
        /// BandName
        /// </summary>
        public static readonly string BandName = "Name: ";

        /// <summary>
        /// BandFormDate
        /// </summary>
        public static readonly string BandFormDate = "Form Date (yyyy): ";

        /*/
         *  LABEL
        /*/

        /// <summary>
        /// Label
        /// </summary>
        public static readonly string Label = "Label: ";

        /// <summary>
        /// Hometown
        /// </summary>
        public static readonly string Hometown = "Hometown: ";

        /// <summary>
        /// BandDetails
        /// </summary>
        public static readonly string[] BandDetails = new string[] { BandName, BandFormDate, Label, Hometown };

        /// <summary>
        /// CreateLabel
        /// </summary>
        public static readonly string CreateLabel = "Create Label";

        /// <summary>
        /// CreateMultipleLabels
        /// </summary>
        public static readonly string CreateMultipleLabels = "Create Multiple Labels";

        /// <summary>
        /// UpdateLabel
        /// </summary>
        public static readonly string UpdateLabel = "Update Label";

        /// <summary>
        /// LabelName
        /// </summary>
        public static readonly string LabelName = "Label Name: ";

        /// <summary>
        /// LabelFormDate
        /// </summary>
        public static readonly string LabelFormDate = "Form Date (yyyy): ";

        /// <summary>
        /// LabelDetails
        /// </summary>
        public static readonly string[] LabelDetails = new string[] { LabelName, BandFormDate };

        /*/
         *  ALBUM
        /*/

        /// <summary>
        /// CreateAlbum
        /// </summary>
        public static readonly string CreateAlbum = "Create Album";

        /// <summary>
        /// CreateMultipleAlbums
        /// </summary>
        public static readonly string CreateMultipleAlbums = "Create Multiple Albums";

        /// <summary>
        /// UpdateAlbum
        /// </summary>
        public static readonly string UpdateAlbum = "Update Album";

        /// <summary>
        /// AlbumName
        /// </summary>
        public static readonly string AlbumName = "Name: ";

        /// <summary>
        /// AlbumYearReleased
        /// </summary>
        public static readonly string AlbumYearReleased = "Release Year (yyyy): ";

        /// <summary>
        /// AlbumLabelName
        /// </summary>
        public static readonly string AlbumLabelName = "Label Name: ";

        /// <summary>
        /// Producer
        /// </summary>
        public static readonly string Producer = "Producer. ";

        /// <summary>
        /// AlbumBandName
        /// </summary>
        public static readonly string AlbumBandName = "Band Name: ";

        /// <summary>
        /// AlbumDeatails
        /// </summary>
        public static readonly string[] AlbumDeatails = new string[] { AlbumName, AlbumYearReleased, AlbumLabelName, Producer, AlbumBandName };

        /*/
         *  MUSICIAN
        /*/

        /// <summary>
        /// CreateMusician
        /// </summary>
        public static readonly string CreateMusician = "Create Muusician";

        /// <summary>
        /// CreateMultipleMusicians
        /// </summary>
        public static readonly string CreateMultipleMusicians = "Create Multiple Musicians";

        /// <summary>
        /// UpdateMusician
        /// </summary>
        public static readonly string UpdateMusician = "Update Musician";

        /// <summary>
        /// MusicianName
        /// </summary>
        public static readonly string MusicianName = "Name: ";

        /// <summary>
        /// DateOfBirth
        /// </summary>
        public static readonly string DateOfBirth = "Date Of Birth (yyyy.mm.dd.): ";

        /// <summary>
        /// MusicianHometown
        /// </summary>
        public static readonly string MusicianHometown = "Hometown: ";

        /// <summary>
        /// MusicianDetails
        /// </summary>
        public static readonly string[] MusicianDetails = new string[] { MusicianName, DateOfBirth, MusicianHometown };

        /*/
         *  SONG
        /*/

        /// <summary>
        /// CreateSong
        /// </summary>
        public static readonly string CreateSong = "Create Song";

        /// <summary>
        /// CreateMultipleSongs
        /// </summary>
        public static readonly string CreateMultipleSongs = "Create Multiple Songs";

        /// <summary>
        /// UpdateSong
        /// </summary>
        public static readonly string UpdateSong = "Update Song";

        /// <summary>
        /// SongBandName
        /// </summary>
        public static readonly string SongBandName = "Band: ";

        /// <summary>
        /// SongAlbumName
        /// </summary>
        public static readonly string SongAlbumName = "Album: ";

        /// <summary>
        /// SongTitle
        /// </summary>
        public static readonly string SongTitle = "Title: ";

        /// <summary>
        /// Genre
        /// </summary>
        public static readonly string Genre = "Genre: ";

        /// <summary>
        /// SongDetails
        /// </summary>
        public static readonly string[] SongDetails = new string[] { SongBandName, SongTitle, SongAlbumName, Genre };

        /// <summary>
        /// SongsByAlbum
        /// </summary>
        public static readonly string SongsByAlbum = "Songs By Album";

        /// <summary>
        /// SongsByBand
        /// </summary>
        public static readonly string SongsByBand = "Songs By Band";

        /// <summary>
        /// BandsByGenre
        /// </summary>
        public static readonly string BandsByGenre = "Bands By Genre";

        /// <summary>
        /// MusiciansByBand
        /// </summary>
        public static readonly string MusiciansByBand = "Musicians By Band";

        /// <summary>
        /// MusiciansOnAlbum
        /// </summary>
        public static readonly string MusiciansOnAlbum = "Musicians On Album";

        /// <summary>
        /// MusiciansUnderLabel
        /// </summary>
        public static readonly string MusiciansUnderLabel = "Musicians Under Label";

        /// <summary>
        /// Method for write standard message for Special Menu items
        /// </summary>
        /// <param name="table1">parameter1</param>
        /// <param name="table2">parameter2</param>
        /// <param name="input">parameter3</param>
        public static void SpecialMenuElements(string table1, string table2, string input)
        {
            Console.WriteLine("************************************\n" +
                              $"List all {table1} on/by {table2}\n" +
                              "************************************\n" +
                             $"Please enter the {input} name\n" +
                              "************************************\n");
        }
    }
}
