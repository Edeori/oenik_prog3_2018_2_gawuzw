﻿// <copyright file="SongException.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <summary>This is a Shared object.</summary>
namespace MusiciansAndBands.Shared
{
    using System;

    /// <summary>
    /// Custom Exception for songs
    /// </summary>
    [Serializable]
    public class SongException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SongException"/> class.
        /// </summary>
        /// <param name="message">Message to throw</param>
        public SongException(string message)
            : base(message)
        {
        }
    }
}
