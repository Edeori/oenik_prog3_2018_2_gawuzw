﻿// <copyright file="LabelException.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <summary>This is a Shared object.</summary>
namespace MusiciansAndBands.Shared
{
    using System;

    /// <summary>
    /// Custom Exception for labels
    /// </summary>
    /// [Serializable]
    public class LabelException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LabelException"/> class.
        /// </summary>
        /// <param name="message">Message to throw</param>
        public LabelException(string message)
            : base(message)
        {
        }
    }
}
