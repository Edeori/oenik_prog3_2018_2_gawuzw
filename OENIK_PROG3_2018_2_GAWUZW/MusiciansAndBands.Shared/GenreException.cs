﻿// <copyright file="GenreException.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <summary>This is a Shared object.</summary>
namespace MusiciansAndBands.Shared
{
    using System;

    /// <summary>
    /// Custom Exception for genres
    /// </summary>
    [Serializable]
    public class GenreException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GenreException"/> class.
        /// </summary>
        /// <param name="message">Message to throw</param>
        public GenreException(string message)
            : base(message)
        {
        }
    }
}
