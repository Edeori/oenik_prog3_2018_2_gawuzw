﻿// <copyright file="BandException.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <summary>This is a Shared object.</summary>
namespace MusiciansAndBands.Shared
{
    using System;

    /// <summary>
    /// Custom Exception for bands
    /// </summary>
    [Serializable]
    public class BandException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BandException"/> class.
        /// </summary>
        /// <param name="message">Message to throw</param>
        public BandException(string message)
            : base(message)
        {
        }
    }
}
