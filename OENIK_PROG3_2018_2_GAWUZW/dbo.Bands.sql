﻿CREATE TABLE [dbo].[Bands] (
    [Band_ID]   INT          NOT NULL IDENTITY,
    [Band_name] VARCHAR (50) NOT NULL,
    [Form_date] DATE         NULL,
    [Label]     VARCHAR (50) NULL,
    [Hometown]  VARCHAR (50) NULL,
    CONSTRAINT [band_pk] PRIMARY KEY CLUSTERED ([Band_ID] ASC),
    CONSTRAINT [label_fk] FOREIGN KEY ([Label]) REFERENCES [dbo].[Labels] ([Label_name])
);

