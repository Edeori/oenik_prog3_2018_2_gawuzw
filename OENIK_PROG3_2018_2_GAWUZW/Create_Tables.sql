﻿CREATE TABLE [dbo].[Labels] (
Label_name VARCHAR(50),
Form_date DATE,
CONSTRAINT [label_name_pk] PRIMARY KEY ([Label_name])
);

CREATE TABLE [dbo].[Bands] (
    [Band_name] VARCHAR (50) NOT NULL,
    [Form_date] DATE         NULL,
    [Label]     VARCHAR (50) NULL,
    [Hometown]  VARCHAR (50) NULL,
    [Band_ID]   INT          IDENTITY (1, 1) NOT NULL,
    CONSTRAINT [band_pk] PRIMARY KEY CLUSTERED ([Band_ID] ASC),
    CONSTRAINT [label_fk] FOREIGN KEY ([Label]) REFERENCES [dbo].[Labels] ([Label_name])
);

CREATE TABLE [dbo].[Albums](
Album_ID INT not null,
Album_name VARCHAR(50),
Year_Released date,
Label VARCHAR(20),
Producer VARCHAR(50),
Band_ID INT not null,
CONSTRAINT [albums_pk] PRIMARY KEY([Album_ID]),
CONSTRAINT [album_label_fk] FOREIGN KEY([Label]) REFERENCES [Labels]([Label_name]),
CONSTRAINT [band_fk] FOREIGN KEY([Band_ID]) REFERENCES [Bands]([Band_ID])
);

CREATE TABLE [dbo].[Musicians](
Musician_name VARCHAR(50) not null,
Date_of_birth date,
Hometown VARCHAR(50),
CONSTRAINT [musician_pk] PRIMARY KEY([Musician_name], [Date_of_birth])
);

CREATE TABLE [dbo].[Songs](
Song_ID INT not null,
Band_ID INT not null,
Album INT not null,
Title VARCHAR(50) not null,
Genre VARCHAR(20),
CONSTRAINT [song_pk] PRIMARY KEY([Song_ID]),
CONSTRAINT [song_band_fk] FOREIGN KEY([Band_ID]) REFERENCES [Bands]([Band_ID]),
CONSTRAINT [album_fk] FOREIGN KEY([Album]) REFERENCES [Albums]([Album_ID])
);

CREATE TABLE [dbo].[Memberships](
Membership_ID INT not null,
Position VARCHAR(20),
Musician_name VARCHAR(50) not null,
Date_of_birth date not null,
Band_ID INT not null,
CONSTRAINT [membership_id_pk] PRIMARY KEY([Membership_ID]),
CONSTRAINT [musician_name_fk] FOREIGN KEY([Musician_name], [Date_of_birth]) REFERENCES [Musicians]([Musician_name], [Date_of_birth]),
CONSTRAINT [Band_ID]  FOREIGN KEY([Band_ID]) REFERENCES [Bands]([Band_ID])
);
