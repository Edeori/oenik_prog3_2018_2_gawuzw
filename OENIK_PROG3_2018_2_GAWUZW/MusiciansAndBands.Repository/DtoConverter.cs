﻿// <copyright file="DtoConverter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <summary>MusiciansAndBands.Repository</summary>
namespace MusiciansAndBands.Repository
{
    using System;
    using System.Linq;
    using MusiciansAndBands.Data;
    using MusiciansAndBands.Dtos;

    /// <summary>
    /// This class made for converting the data transfer objects to entities
    /// and from entities to data transfer objects.
    /// </summary>
    public static class DtoConverter
    {
        /// <summary>
        /// Converter for band DTO => Entity
        /// </summary>
        /// <param name="bandDto">Filled data transfer object</param>
        /// <param name="band">Empty Entity</param>
        public static void ConvertBandDtoToEntity(BandDto bandDto, Bands band)
        {
            band.Band_name = bandDto.Band_name;
            band.Label = bandDto.Label;
            band.Form_date = bandDto.Form_date;
            band.Hometown = bandDto.Hometown;
        }

        /// <summary>
        /// Converter for album DTO => Entity
        /// </summary>
        /// <param name="albumDto">Filled data transfer object</param>
        /// <param name="album">Empty Entity</param>
        public static void ConvertAlbumDtoToEntity(AlbumDto albumDto, Albums album)
        {
            album.Album_name = albumDto.Album_name;
            if (albumDto.Album_ID != 0)
            {
                album.Album_ID = albumDto.Album_ID;
            }

            album.Year_Released = albumDto.Year_Released;
            album.Label = albumDto.Label;
            album.Producer = albumDto.Producer;
            if (albumDto.BandId != null)
            {
                album.Band_ID = int.Parse(albumDto.BandId);
            }
            else
            {
                using (MusiciansAndBandsDbEntities1 musiciansAndBandsDbEntities = new MusiciansAndBandsDbEntities1())
                {
                    album.Band_ID = (from band in musiciansAndBandsDbEntities.Bands
                                     where band.Band_name == albumDto.BandName
                                     select band.Band_ID).FirstOrDefault();
                }
            }
        }

        /// <summary>
        /// Converter for label DTO => Entity
        /// </summary>
        /// <param name="labelDto">Filled data transfer object</param>
        /// <param name="label">Empty Entity</param>
        public static void ConvertLabelDtoToEntity(LabelDto labelDto, Labels label)
        {
            label.Label_name = labelDto.Label_name;
            label.Form_date = labelDto.Form_date;
        }

        /// <summary>
        /// Converter for musician DTO => Entity
        /// </summary>
        /// <param name="musicianDto">Filled data transfer object</param>
        /// <param name="musician">Empty Entity</param>
        public static void ConvertMusicianDtoToEntity(MusicianDto musicianDto, Musicians musician)
        {
            musician.Musician_name = musicianDto.Musician_name;
            musician.Date_of_birth = musicianDto.Date_of_birth;
            musician.Hometown = musicianDto.Hometown;
        }

        /// <summary>
        /// Converter for song DTO => Entity
        /// </summary>
        /// <param name="songDto">Filled data transfer object</param>
        /// <param name="song">Empty Entity</param>
        public static void ConvertSongDtoToEntity(SongDto songDto, Songs song)
        {
            song.Title = songDto.Title;
            song.Genre = songDto.Genre;
            using (MusiciansAndBandsDbEntities1 musiciansAndBandsDbEntities = new MusiciansAndBandsDbEntities1())
            {
                if (songDto.Album_ID != 0 || songDto.Album == null)
                {
                    song.Album = songDto.Album_ID;
                }
                else
                {
                    song.Album = (from album in musiciansAndBandsDbEntities.Albums
                                  where album.Album_name == songDto.Album
                                  select album.Album_ID).FirstOrDefault();
                }

                if (songDto.Band_ID != 0)
                {
                    song.Band_ID = songDto.Band_ID;
                }
                else
                {
                    song.Band_ID = (from album in musiciansAndBandsDbEntities.Albums
                                    where album.Album_ID == songDto.Album_ID
                                    select album.Band_ID).FirstOrDefault();
                }
            }
        }

        /// <summary>
        /// Converter for band Entity => DTO
        /// </summary>
        /// <param name="bandDto">Empty data transfer object</param>
        /// <param name="band">Filled Entity</param>
        public static void ConvertBandEntityToDto(BandDto bandDto, Bands band)
        {
            bandDto.Band_name = band.Band_name;
            bandDto.Label = band.Label;
            bandDto.Form_date = (DateTime)band.Form_date;
            bandDto.Hometown = band.Hometown;
        }

        /// <summary>
        /// Converter for membership DTO => Entity
        /// </summary>
        /// <param name="membershipDto">Filled data transfer object</param>
        /// <param name="membership">Empty Entity</param>
        internal static void ConvertMembershipDtoToEntity(MembershipDto membershipDto, Memberships membership)
        {
            using (MusiciansAndBandsDbEntities1 musiciansAndBandsDbEntities = new MusiciansAndBandsDbEntities1())
            {
                if (membershipDto.Band_ID != 0)
                {
                    membership.Band_ID = membershipDto.Band_ID;
                }
                else
                {
                    membership.Band_ID = (from band in musiciansAndBandsDbEntities.Bands
                                          where band.Band_name == membershipDto.Band
                                          select band.Band_ID).FirstOrDefault();
                }
            }

            membership.Date_of_birth = membershipDto.Date_of_birth;
            membership.Musician_name = membershipDto.Musician_name;
            membership.Position = membershipDto.Position;
        }
    }
}
