﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <summary>This is the Repository.</summary>
namespace MusiciansAndBands.Repository
{
    using System.Collections.Generic;
    using MusiciansAndBands.Dtos;

    /// <summary>
    /// Interface for Repository
    /// </summary>
    public interface IRepository
    {
        /// <summary>
        /// Create method for band in database
        /// </summary>
        /// <param name="bandDto">New band data transfer object</param>
        void CreateNewBand(BandDto bandDto);

        /// <summary>
        /// Create method for album in database
        /// </summary>
        /// <param name="albumDto">New album data transfer object</param>
        void CreateNewAlbum(AlbumDto albumDto);

        /// <summary>
        /// Create method for label in database
        /// </summary>
        /// <param name="labelDto">New label data transfer object</param>
        void CreateNewLabel(LabelDto labelDto);

        /// <summary>
        /// Create method for musician in database
        /// </summary>
        /// <param name="musicianDto">New musician data transfer object</param>
        void CreateNewMusician(MusicianDto musicianDto);

        /// <summary>
        /// Create method for song in database
        /// </summary>
        /// <param name="songDto">New song data transfer object</param>
        void CreateNewSong(SongDto songDto);

        /// <summary>
        /// Create method for membership in database
        /// </summary>
        /// <param name="membershipDto">New membership data transfer object</param>
        void CreateNewMembership(MembershipDto membershipDto);

        /// <summary>
        /// Get method from band table by id
        /// </summary>
        /// <param name="id">Id of the requested database record</param>
        /// <returns>Data transfer object of the request database record</returns>
        BandDto GetBandById(int id);

        /// <summary>
        /// Get method from album table by id
        /// </summary>
        /// <param name="id">Id of the requested database record</param>
        /// <returns>Data transfer object of the request database record</returns>
        AlbumDto GetAlbumById(int id);

        /// <summary>
        /// Get method from label table by id
        /// </summary>
        /// <param name="labelName">Id of the requested database record</param>
        /// <returns>Data transfer object of the request database record</returns>
        LabelDto GetLabelById(string labelName);

        /// <summary>
        /// Get method from musician table by id
        /// </summary>
        /// <param name="musicianName">Id of the requested database record</param>
        /// <returns>Data transfer object of the request database record</returns>
        MusicianDto GetMusicianById(string musicianName);

        /// <summary>
        /// Get method from song table by id
        /// </summary>
        /// <param name="id">Id of the requested database record</param>
        /// <returns>Data transfer object of the request database record</returns>
        SongDto GetSongById(int id);

        /// <summary>
        /// Get method from membership table by id
        /// </summary>
        /// <param name="id">Id of the requested database record</param>
        /// <returns>Data transfer object of the request database record</returns>
        MembershipDto GetMembershipById(int id);

        /// <summary>
        /// Update an existing band record in the database
        /// </summary>
        /// <param name="id">Id of the requested database record</param>
        /// <param name="bandDto">Data transfer object with the update value/s</param>
        void UpdateBandById(int id, BandDto bandDto);

        /// <summary>
        /// Update an existing album record in the database
        /// </summary>
        /// <param name="id">Id of the requested database record</param>
        /// <param name="albumDto">Data transfer object with the update value/s</param>
        void UpdateAlbumById(int id, AlbumDto albumDto);

        /// <summary>
        /// Update an existing label record in the database
        /// </summary>
        /// <param name="labelName">Id of the requested database record</param>
        /// <param name="labelDto">Data transfer object with the update value/s</param>
        void UpdateLabelById(string labelName, LabelDto labelDto);

        /// <summary>
        /// Update an existing musician record in the database
        /// </summary>
        /// <param name="musicianName">Id of the requested database record</param>
        /// <param name="musicianDto">Data transfer object with the update value/s</param>
        void UpdateMusicianById(string musicianName, MusicianDto musicianDto);

        /// <summary>
        /// Update an existing song record in the database
        /// </summary>
        /// <param name="id">Id of the requested database record</param>
        /// <param name="songDto">Data transfer object with the update value/s</param>
        void UpdateSongById(int id, SongDto songDto);

        /// <summary>
        /// Update an existing membership record in the database
        /// </summary>
        /// <param name="id">Id of the requested database record</param>
        /// <param name="membershipDto">Data transfer object with the update value/s</param>
        void UpdateMembershipById(int id, MembershipDto membershipDto);

        /// <summary>
        /// Delete an existing record from the band table in the database by Id
        /// </summary>
        /// <param name="id">Id of the deletable database record</param>
        void DeleteBandById(int id);

        /// <summary>
        /// Delete an existing record from the album table in the database by Id
        /// </summary>
        /// <param name="id">Id of the deletable database record</param>
        void DeleteAlbumById(int id);

        /// <summary>
        /// Delete an existing record from the label table in the database by Id
        /// </summary>
        /// <param name="labelName">Id of the deletable database record</param>
        void DeleteLabelById(string labelName);

        /// <summary>
        /// Delete an existing record from the musician table in the database by Id
        /// </summary>
        /// <param name="musicianName">Id of the deletable database record</param>
        void DeleteMusicianById(string musicianName);

        /// <summary>
        /// Delete an existing record from the song table in the database by Id
        /// </summary>
        /// <param name="id">Id of the deletable database record</param>
        void DeleteSongById(int id);

        /// <summary>
        /// Delete an existing record from the membership table in the database by Id
        /// </summary>
        /// <param name="id">Id of the deletable database record</param>
        void DeleteMembershipById(int id);

        /// <summary>
        /// List all records in the band table
        /// </summary>
        /// <returns>List of the bands in database</returns>
        List<BandDto> ListAllBands();

        /// <summary>
        /// List all records in the album table
        /// </summary>
        /// <returns>List of the albums in database</returns>
        List<AlbumDto> ListAllAlbums();

        /// <summary>
        /// List all records in the label table
        /// </summary>
        /// <returns>List of the labels in database</returns>
        List<LabelDto> ListAllLabels();

        /// <summary>
        /// List all records in the musician table
        /// </summary>
        /// <returns>List of the musicians in database</returns>
        List<MusicianDto> ListAllMusicians();

        /// <summary>
        /// List all records in the song table
        /// </summary>
        /// <returns>List of the songs in database</returns>
        List<SongDto> ListAllSongs();

        /// <summary>
        /// List all records in the membership table
        /// </summary>
        /// <returns>List of the memberships in database</returns>
        List<MembershipDto> ListAllMemberships();
    }
}
