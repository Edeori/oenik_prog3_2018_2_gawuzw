﻿// <copyright file="RepositoryImpl.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <summary>This is the Repository.</summary>
namespace MusiciansAndBands.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using MusiciansAndBands.Data;
    using MusiciansAndBands.Dtos;
    using MusiciansAndBands.Shared;

    /// <summary>
    /// Implementation of <see cref="IRepository"/>
    /// </summary>
    public class RepositoryImpl : IRepository
    {
        /// <summary>
        /// Implementation of <see cref="IRepository.CreateNewAlbum"/>
        /// </summary>
        /// <param name="albumDto">New album data transfer object</param>
        public void CreateNewAlbum(AlbumDto albumDto)
        {
            Albums album = new Albums();
            DtoConverter.ConvertAlbumDtoToEntity(albumDto, album);
            if (albumDto.Album_ID == 0 || GetAlbumById(albumDto.Album_ID) == null)
            {
                using (MusiciansAndBandsDbEntities1 musiciansAndBandsDbEntities = new MusiciansAndBandsDbEntities1())
                {
                    musiciansAndBandsDbEntities.Albums.Add(album);
                    musiciansAndBandsDbEntities.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Implementation of <see cref="IRepository.CreateNewBand"/>
        /// </summary>
        /// <param name="bandDto">New band data transfer object</param>
        public void CreateNewBand(BandDto bandDto)
        {
            Bands bands = new Bands();
            DtoConverter.ConvertBandDtoToEntity(bandDto, bands);
            using (MusiciansAndBandsDbEntities1 musiciansAndBandsDbEntities = new MusiciansAndBandsDbEntities1())
            {
                musiciansAndBandsDbEntities.Bands.Add(bands);
                musiciansAndBandsDbEntities.SaveChanges();
            }
        }

        /// <summary>
        /// Implementation of <see cref="IRepository.CreateNewLabel"/>
        /// </summary>
        /// <param name="labelDto">New label data transfer object</param>
        public void CreateNewLabel(LabelDto labelDto)
        {
            Labels label = new Labels();
            DtoConverter.ConvertLabelDtoToEntity(labelDto, label);
            if (GetLabelById(labelDto.Label_name) == null)
            {
                using (MusiciansAndBandsDbEntities1 musiciansAndBandsDbEntities = new MusiciansAndBandsDbEntities1())
                {
                    musiciansAndBandsDbEntities.Labels.Add(label);
                    musiciansAndBandsDbEntities.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Implementation of <see cref="IRepository.CreateNewMembership"/>
        /// </summary>
        /// <param name="membershipDto">New membership data transfer object</param>
        public void CreateNewMembership(MembershipDto membershipDto)
        {
            Memberships membership = new Memberships();
            DtoConverter.ConvertMembershipDtoToEntity(membershipDto, membership);
            if (GetMembershipById(membershipDto.Membership_ID) == null)
            {
                using (MusiciansAndBandsDbEntities1 musiciansAndBandsDbEntities = new MusiciansAndBandsDbEntities1())
                {
                    musiciansAndBandsDbEntities.Memberships.Add(membership);
                    musiciansAndBandsDbEntities.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Implementation of <see cref="IRepository.CreateNewMusician"/>
        /// </summary>
        /// <param name="musicianDto">New musician data transfer object</param>
        public void CreateNewMusician(MusicianDto musicianDto)
        {
            Musicians musician = new Musicians();
            DtoConverter.ConvertMusicianDtoToEntity(musicianDto, musician);
            if (GetMusicianById(musicianDto.Musician_name) == null)
            {
                using (MusiciansAndBandsDbEntities1 musiciansAndBandsDbEntities = new MusiciansAndBandsDbEntities1())
                {
                    musiciansAndBandsDbEntities.Musicians.Add(musician);
                    musiciansAndBandsDbEntities.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Implementation of <see cref="IRepository.CreateNewSong"/>
        /// </summary>
        /// <param name="songDto">New song data transfer object</param>
        public void CreateNewSong(SongDto songDto)
        {
            Songs song = new Songs();
            DtoConverter.ConvertSongDtoToEntity(songDto, song);
            if (GetSongById(songDto.Song_ID) == null)
            {
                using (MusiciansAndBandsDbEntities1 musiciansAndBandsDbEntities = new MusiciansAndBandsDbEntities1())
                {
                    musiciansAndBandsDbEntities.Songs.Add(song);
                    musiciansAndBandsDbEntities.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Implementation of <see cref="IRepository.DeleteAlbumById"/>
        /// </summary>
        /// <param name="id">Id of the deletable database record</param>
        public void DeleteAlbumById(int id)
        {
            using (MusiciansAndBandsDbEntities1 musiciansAndBandsDbEntities = new MusiciansAndBandsDbEntities1())
            {
                var itemToRemove = musiciansAndBandsDbEntities.Albums.SingleOrDefault(x => x.Album_ID == id);
                if (itemToRemove != null)
                {
                    musiciansAndBandsDbEntities.Albums.Remove(itemToRemove);
                    musiciansAndBandsDbEntities.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Implementation of <see cref="IRepository.DeleteBandById"/>
        /// </summary>
        /// <param name="id">Id of the deletable database record</param>
        public void DeleteBandById(int id)
        {
            using (MusiciansAndBandsDbEntities1 musiciansAndBandsDbEntities = new MusiciansAndBandsDbEntities1())
            {
                var itemToRemove = musiciansAndBandsDbEntities.Bands.SingleOrDefault(x => x.Band_ID == id);
                if (itemToRemove != null)
                {
                    musiciansAndBandsDbEntities.Bands.Remove(itemToRemove);
                    musiciansAndBandsDbEntities.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Implementation of <see cref="IRepository.DeleteLabelById"/>
        /// </summary>
        /// <param name="labelName">Id of the deletable database record</param>
        public void DeleteLabelById(string labelName)
        {
            using (MusiciansAndBandsDbEntities1 musiciansAndBandsDbEntities = new MusiciansAndBandsDbEntities1())
            {
                var itemToRemove = musiciansAndBandsDbEntities.Labels.SingleOrDefault(x => x.Label_name == labelName);
                if (itemToRemove != null)
                {
                    musiciansAndBandsDbEntities.Labels.Remove(itemToRemove);
                    musiciansAndBandsDbEntities.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Implementation of <see cref="IRepository.DeleteMembershipById"/>
        /// </summary>
        /// <param name="id">Id of the deletable database record</param>
        public void DeleteMembershipById(int id)
        {
            using (MusiciansAndBandsDbEntities1 musiciansAndBandsDbEntities = new MusiciansAndBandsDbEntities1())
            {
                var itemToRemove = musiciansAndBandsDbEntities.Memberships.SingleOrDefault(x => x.Membership_ID == id);
                if (itemToRemove != null)
                {
                    musiciansAndBandsDbEntities.Memberships.Remove(itemToRemove);
                    musiciansAndBandsDbEntities.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Implementation of <see cref="IRepository.DeleteMusicianById"/>
        /// </summary>
        /// <param name="musicianName">Id of the deletable database record</param>
        public void DeleteMusicianById(string musicianName)
        {
            using (MusiciansAndBandsDbEntities1 musiciansAndBandsDbEntities = new MusiciansAndBandsDbEntities1())
            {
                var itemToRemove = musiciansAndBandsDbEntities.Musicians.SingleOrDefault(x => x.Musician_name == musicianName);
                if (itemToRemove != null)
                {
                    musiciansAndBandsDbEntities.Musicians.Remove(itemToRemove);
                    musiciansAndBandsDbEntities.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Implementation of <see cref="IRepository.DeleteSongById"/>
        /// </summary>
        /// <param name="id">Id of the deletable database record</param>
        public void DeleteSongById(int id)
        {
            using (MusiciansAndBandsDbEntities1 musiciansAndBandsDbEntities = new MusiciansAndBandsDbEntities1())
            {
                var itemToRemove = musiciansAndBandsDbEntities.Songs.SingleOrDefault(x => x.Song_ID == id);
                if (itemToRemove != null)
                {
                    musiciansAndBandsDbEntities.Songs.Remove(itemToRemove);
                    musiciansAndBandsDbEntities.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Implementation of <see cref="IRepository.GetAlbumById"/>
        /// </summary>
        /// <param name="id">Id of the requested database record</param>
        /// <returns>Data transfer object of the request database record</returns>
        public AlbumDto GetAlbumById(int id)
        {
            using (MusiciansAndBandsDbEntities1 musiciansAndBandsDbEntities = new MusiciansAndBandsDbEntities1())
            {
                AlbumDto albumById;
                try
                {
                    albumById = (from album in musiciansAndBandsDbEntities.Albums
                                 where album.Album_ID == id
                                 select new AlbumDto()
                                 {
                                     Album_name = album.Album_name,
                                     Label = album.Label,
                                     BandName = (from band in musiciansAndBandsDbEntities.Bands
                                                 where band.Band_ID == album.Band_ID
                                                 select band.Band_name).FirstOrDefault(),
                                     Producer = album.Producer,
                                     Year_Released = (DateTime)album.Year_Released
                                 }).FirstOrDefault();
                }
                catch (AlbumException ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.ReadLine();
                    return null;
                }

                return albumById;
            }
        }

        /// <summary>
        /// Implementation of <see cref="IRepository.GetBandById"/>
        /// </summary>
        /// <param name="id">Id of the requested database record</param>
        /// <returns>Data transfer object of the request database record</returns>
        public BandDto GetBandById(int id)
        {
            using (MusiciansAndBandsDbEntities1 musiciansAndBandsDbEntities = new MusiciansAndBandsDbEntities1())
            {
                BandDto bandById;

                try
                {
                    bandById = (from band in musiciansAndBandsDbEntities.Bands
                                where band.Band_ID == id
                                select new BandDto()
                                {
                                    Band_name = band.Band_name,
                                    Form_date = (DateTime)band.Form_date,
                                    Hometown = band.Hometown,
                                    Label = band.Label
                                }).Single();
                }
                catch (BandException ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.ReadLine();
                    return null;
                }

                return bandById;
            }
        }

        /// <summary>
        /// Implementation of <see cref="IRepository.GetLabelById"/>
        /// </summary>
        /// <param name="labelName">Id of the requested database record</param>
        /// <returns>Data transfer object of the request database record</returns>
        public LabelDto GetLabelById(string labelName)
        {
            using (MusiciansAndBandsDbEntities1 musiciansAndBandsDbEntities = new MusiciansAndBandsDbEntities1())
            {
                LabelDto labelById;

                try
                {
                    labelById = (from label in musiciansAndBandsDbEntities.Labels
                                 where label.Label_name == labelName
                                 select new LabelDto()
                                 {
                                     Label_name = label.Label_name,
                                     Form_date = (DateTime)label.Form_date
                                 }).FirstOrDefault();
                }
                catch (LabelException ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.ReadLine();
                    return null;
                }

                return labelById;
            }
        }

        /// <summary>
        /// Implementation of <see cref="IRepository.GetMembershipById"/>
        /// </summary>
        /// <param name="id">Id of the requested database record</param>
        /// <returns>Data transfer object of the request database record</returns>
        public MembershipDto GetMembershipById(int id)
        {
            using (MusiciansAndBandsDbEntities1 musiciansAndBandsDbEntities = new MusiciansAndBandsDbEntities1())
            {
                MembershipDto membershipById;

                try
                {
                    membershipById = (from membership in musiciansAndBandsDbEntities.Memberships
                                      where membership.Membership_ID == id
                                      select new MembershipDto()
                                      {
                                          Membership_ID = membership.Membership_ID,
                                          Musician_name = membership.Musician_name,
                                          Date_of_birth = membership.Date_of_birth,
                                          Position = membership.Position,
                                          Band_ID = membership.Band_ID,
                                          Band = (from band in musiciansAndBandsDbEntities.Bands
                                                  where band.Band_ID == membership.Band_ID
                                                  select band.Band_name).FirstOrDefault()
                                      }).FirstOrDefault();
                }
                catch (MembersipException ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.ReadLine();
                    return null;
                }

                return membershipById;
            }
        }

        /// <summary>
        /// Implementation of <see cref="IRepository.GetMusicianById"/>
        /// </summary>
        /// <param name="musicianName">Id of the requested database record</param>
        /// <returns>Data transfer object of the request database record</returns>
        public MusicianDto GetMusicianById(string musicianName)
        {
            using (MusiciansAndBandsDbEntities1 musiciansAndBandsDbEntities = new MusiciansAndBandsDbEntities1())
            {
                MusicianDto musicianById;

                try
                {
                    musicianById = (from musician in musiciansAndBandsDbEntities.Musicians
                                    where musician.Musician_name == musicianName
                                    select new MusicianDto()
                                    {
                                        Musician_name = musician.Musician_name,
                                        Date_of_birth = musician.Date_of_birth,
                                        Hometown = musician.Hometown
                                    }).FirstOrDefault();
                }
                catch (MusicianException ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.ReadLine();
                    return null;
                }

                return musicianById;
            }
        }

        /// <summary>
        /// Implementation of <see cref="IRepository.GetSongById"/>
        /// </summary>
        /// <param name="id">Id of the requested database record</param>
        /// <returns>Data transfer object of the request database record</returns>
        public SongDto GetSongById(int id)
        {
            using (MusiciansAndBandsDbEntities1 musiciansAndBandsDbEntities = new MusiciansAndBandsDbEntities1())
            {
                SongDto songById;

                try
                {
                    songById = (from song in musiciansAndBandsDbEntities.Songs
                                where song.Song_ID == id
                                select new SongDto()
                                {
                                    Band = (from band in musiciansAndBandsDbEntities.Bands
                                            where band.Band_ID == song.Band_ID
                                            select band.Band_name).FirstOrDefault(),
                                    Album = (from album in musiciansAndBandsDbEntities.Albums
                                             where album.Album_ID == song.Album
                                             select album.Album_name).FirstOrDefault(),
                                    Genre = song.Genre,
                                    Title = song.Title
                                }).FirstOrDefault();
                }
                catch (SongException ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.ReadLine();
                    return null;
                }

                return songById;
            }
        }

        /// <summary>
        /// Implementation of <see cref="IRepository.ListAllAlbums"/>
        /// </summary>
        /// <returns>List of the albums in database</returns>
        public List<AlbumDto> ListAllAlbums()
        {
            using (MusiciansAndBandsDbEntities1 musiciansAndBandsDbEntities = new MusiciansAndBandsDbEntities1())
            {
                var allAlbumsList = (from album in musiciansAndBandsDbEntities.Albums
                                     select new AlbumDto()
                                     {
                                         Album_ID = album.Album_ID,
                                         Album_name = album.Album_name,
                                         Label = album.Label,
                                         BandName = (from band in musiciansAndBandsDbEntities.Bands
                                                     where band.Band_ID == album.Band_ID
                                                     select band.Band_name).FirstOrDefault(),
                                         Producer = album.Producer,
                                         Year_Released = (DateTime)album.Year_Released
                                     }).ToList();

                return allAlbumsList;
            }
        }

        /// <summary>
        /// Implementation of <see cref="IRepository.ListAllBands"/>
        /// </summary>
        /// <returns>List of the bands in database</returns>
        public List<BandDto> ListAllBands()
        {
            using (MusiciansAndBandsDbEntities1 musiciansAndBandsDbEntities = new MusiciansAndBandsDbEntities1())
            {
                var allBandsList = (from band in musiciansAndBandsDbEntities.Bands
                                    select new BandDto()
                                    {
                                        Band_ID = band.Band_ID,
                                        Band_name = band.Band_name,
                                        Form_date = (DateTime)band.Form_date,
                                        Hometown = band.Hometown,
                                        Label = band.Label
                                    }).ToList();

                return allBandsList;
            }
        }

        /// <summary>
        /// Implementation of <see cref="IRepository.ListAllLabels"/>
        /// </summary>
        /// <returns>List of the labels in database</returns>
        public List<LabelDto> ListAllLabels()
        {
            using (MusiciansAndBandsDbEntities1 musiciansAndBandsDbEntities = new MusiciansAndBandsDbEntities1())
            {
                var allLabelsList = (from label in musiciansAndBandsDbEntities.Labels
                                     select new LabelDto()
                                     {
                                         Label_name = label.Label_name,
                                         Form_date = (DateTime)label.Form_date
                                     }).ToList();

                return allLabelsList;
            }
        }

        /// <summary>
        /// Implementation of <see cref="IRepository.ListAllMemberships"/>
        /// </summary>
        /// <returns>List of the memberships in database</returns>
        public List<MembershipDto> ListAllMemberships()
        {
            using (MusiciansAndBandsDbEntities1 musiciansAndBandsDbEntities = new MusiciansAndBandsDbEntities1())
            {
                var allMembershipsList = (from membership in musiciansAndBandsDbEntities.Memberships
                                          select new MembershipDto()
                                          {
                                              Membership_ID = membership.Membership_ID,
                                              Musician_name = membership.Musician_name,
                                              Date_of_birth = membership.Date_of_birth,
                                              Position = membership.Position,
                                              Band_ID = membership.Band_ID,
                                              Band = (from band in musiciansAndBandsDbEntities.Bands
                                                      where band.Band_ID == membership.Band_ID
                                                      select band.Band_name).FirstOrDefault()
                                          }).ToList();

                return allMembershipsList;
            }
        }

        /// <summary>
        /// Implementation of <see cref="IRepository.ListAllMusicians"/>
        /// </summary>
        /// <returns>List of the musicians in database</returns>
        public List<MusicianDto> ListAllMusicians()
        {
            using (MusiciansAndBandsDbEntities1 musiciansAndBandsDbEntities = new MusiciansAndBandsDbEntities1())
            {
                var allMusiciansList = (from musician in musiciansAndBandsDbEntities.Musicians
                                        select new MusicianDto()
                                        {
                                            Musician_name = musician.Musician_name,
                                            Date_of_birth = musician.Date_of_birth,
                                            Hometown = musician.Hometown
                                        }).ToList();

                return allMusiciansList;
            }
        }

        /// <summary>
        /// Implementation of <see cref="IRepository.ListAllSongs"/>
        /// </summary>
        /// <returns>List of the songs in database</returns>
        public List<SongDto> ListAllSongs()
        {
            using (MusiciansAndBandsDbEntities1 musiciansAndBandsDbEntities = new MusiciansAndBandsDbEntities1())
            {
                var allSongsList = (from song in musiciansAndBandsDbEntities.Songs
                                    select new SongDto()
                                    {
                                        Band = (from band in musiciansAndBandsDbEntities.Bands
                                                where band.Band_ID == song.Band_ID
                                                select band.Band_name).FirstOrDefault(),
                                        Album = (from album in musiciansAndBandsDbEntities.Albums
                                                 where album.Album_ID == song.Album
                                                 select album.Album_name).FirstOrDefault(),
                                        Genre = song.Genre,
                                        Title = song.Title,
                                        Song_ID = song.Song_ID
                                    }).ToList();

                return allSongsList;
            }
        }

        /// <summary>
        /// Implementation of <see cref="IRepository.UpdateAlbumById"/>
        /// </summary>
        /// <param name="id">Id of the requested database record</param>
        /// <param name="albumDto">Data transfer object with the update value/s</param>
        public void UpdateAlbumById(int id, AlbumDto albumDto)
        {
            using (MusiciansAndBandsDbEntities1 musiciansAndBandsDbEntities = new MusiciansAndBandsDbEntities1())
            {
                var item = musiciansAndBandsDbEntities.Albums.SingleOrDefault(x => x.Album_ID == id);
                if (item != null)
                {
                    musiciansAndBandsDbEntities.Entry(item).CurrentValues.SetValues(albumDto);
                    musiciansAndBandsDbEntities.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Implementation of <see cref="IRepository.UpdateBandById"/>
        /// </summary>
        /// <param name="id">Id of the requested database record</param>
        /// <param name="bandDto">Data transfer object with the update value/s</param>
        public void UpdateBandById(int id, BandDto bandDto)
        {
            using (MusiciansAndBandsDbEntities1 musiciansAndBandsDbEntities = new MusiciansAndBandsDbEntities1())
            {
                var item = musiciansAndBandsDbEntities.Bands.SingleOrDefault(x => x.Band_ID == id);
                if (item != null)
                {
                    musiciansAndBandsDbEntities.Entry(item).CurrentValues.SetValues(bandDto);
                    musiciansAndBandsDbEntities.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Implementation of <see cref="IRepository.UpdateLabelById"/>
        /// </summary>
        /// <param name="labelName">Id of the requested database record</param>
        /// <param name="labelDto">Data transfer object with the update value/s</param>
        public void UpdateLabelById(string labelName, LabelDto labelDto)
        {
            using (MusiciansAndBandsDbEntities1 musiciansAndBandsDbEntities = new MusiciansAndBandsDbEntities1())
            {
                var item = musiciansAndBandsDbEntities.Labels.SingleOrDefault(x => x.Label_name == labelName);
                if (item != null)
                {
                    musiciansAndBandsDbEntities.Entry(item).CurrentValues.SetValues(labelDto);
                    musiciansAndBandsDbEntities.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Implementation of <see cref="IRepository.UpdateMembershipById"/>
        /// </summary>
        /// <param name="id">Id of the requested database record</param>
        /// <param name="membershipDto">Data transfer object with the update value/s</param>
        public void UpdateMembershipById(int id, MembershipDto membershipDto)
        {
            using (MusiciansAndBandsDbEntities1 musiciansAndBandsDbEntities = new MusiciansAndBandsDbEntities1())
            {
                var item = musiciansAndBandsDbEntities.Memberships.SingleOrDefault(x => x.Membership_ID == id);
                if (item != null)
                {
                    musiciansAndBandsDbEntities.Entry(item).CurrentValues.SetValues(membershipDto);
                    musiciansAndBandsDbEntities.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Implementation of <see cref="IRepository.UpdateMusicianById"/>
        /// </summary>
        /// <param name="musicianName">Id of the requested database record</param>
        /// <param name="musicianDto">Data transfer object with the update value/s</param>
        public void UpdateMusicianById(string musicianName, MusicianDto musicianDto)
        {
            using (MusiciansAndBandsDbEntities1 musiciansAndBandsDbEntities = new MusiciansAndBandsDbEntities1())
            {
                var item = musiciansAndBandsDbEntities.Musicians.SingleOrDefault(x => x.Musician_name == musicianName);
                if (item != null)
                {
                    musiciansAndBandsDbEntities.Entry(item).CurrentValues.SetValues(musicianDto);
                    musiciansAndBandsDbEntities.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Implementation of <see cref="IRepository.UpdateSongById"/>
        /// </summary>
        /// <param name="id">Id of the requested database record</param>
        /// <param name="songDto">Data transfer object with the update value/s</param>
        public void UpdateSongById(int id, SongDto songDto)
        {
            using (MusiciansAndBandsDbEntities1 musiciansAndBandsDbEntities = new MusiciansAndBandsDbEntities1())
            {
                var item = musiciansAndBandsDbEntities.Songs.SingleOrDefault(x => x.Song_ID == id);
                if (item != null)
                {
                    musiciansAndBandsDbEntities.Entry(item).CurrentValues.SetValues(songDto);
                    musiciansAndBandsDbEntities.SaveChanges();
                }
            }
        }
    }
}
