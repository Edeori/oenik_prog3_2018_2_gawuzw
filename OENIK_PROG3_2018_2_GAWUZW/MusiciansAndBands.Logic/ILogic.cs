﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <summary>This is the Logic.</summary>
namespace MusiciansAndBands.Logic
{
    using System.Collections.Generic;
    using MusiciansAndBands.Dtos;
    using MusiciansAndBands.Repository;

    /// <summary>
    /// Interface for LogicImpl
    /// </summary>
    public interface ILogic
    {
        /// <summary>
        /// Forwarder method for <see cref="IRepository.ListAllBands"/>
        /// </summary>
        /// <returns>List of the bands in database</returns>
        List<BandDto> ListAllBands();

        /// <summary>
        /// Forwarder method for <see cref="IRepository.ListAllAlbums"/>
        /// </summary>
        /// <returns>List of the albums in database</returns>
        List<AlbumDto> ListAllAlbums();

        /// <summary>
        /// Forwarder method for <see cref="IRepository.ListAllLabels"/>
        /// </summary>
        /// <returns>List of the labels in database</returns>
        List<LabelDto> ListAllLabels();

        /// <summary>
        /// Forwarder method for <see cref="IRepository.ListAllMusicians"/>
        /// </summary>
        /// <returns>List of the musicians in database</returns>
        List<MusicianDto> ListAllMusicians();

        /// <summary>
        /// Forwarder method for <see cref="IRepository.ListAllSongs"/>
        /// </summary>
        /// <returns>List of the songs in database</returns>
        List<SongDto> ListAllSongs();

        /// <summary>
        /// Forwarder method for <see cref="IRepository.CreateNewBand"/>
        /// </summary>
        /// <param name="dto">Data transfer object</param>
        void AddNewBand(BandDto dto);

        /// <summary>
        /// Forwarder method for <see cref="IRepository.CreateNewLabel"/>
        /// </summary>
        /// <param name="dto">Data transfer object</param>
        void AddNewLabel(LabelDto dto);

        /// <summary>
        /// Forwarder method for <see cref="IRepository.CreateNewAlbum"/>
        /// </summary>
        /// <param name="dto">Data transfer object</param>
        void AddNewAlbum(AlbumDto dto);

        /// <summary>
        /// Forwarder method for <see cref="IRepository.CreateNewMusician"/>
        /// </summary>
        /// <param name="dto">Data transfer object</param>
        void AddNewMusician(MusicianDto dto);

        /// <summary>
        /// Forwarder method for <see cref="IRepository.CreateNewSong"/>
        /// </summary>
        /// <param name="dto">Data transfer object</param>
        void AddNewSong(SongDto dto);

        /// <summary>
        /// Update existing band record in the database with inputs from the console
        /// </summary>
        /// <param name="id">Id of the requested database record</param>
        /// <param name="dto">New dto of the requested database record</param>
        void UpdateBandById(string id, BandDto dto);

        /// <summary>
        /// Update existing album record in the database with inputs from the console
        /// </summary>
        /// <param name="id">Id of the requested database record</param>
        /// <param name="dto">New dto of the requested database record</param>
        void UpdateAlbumById(string id, AlbumDto dto);

        /// <summary>
        /// Update existing label record in the database with inputs from the console
        /// </summary>
        /// <param name="id">Id of the requested database record</param>
        /// <param name="dto">New dto of the requested database record</param>
        void UpdateLabelById(string id, LabelDto dto);

        /// <summary>
        /// Update existing musician record in the database with inputs from the console
        /// </summary>
        /// <param name="id">Id of the requested database record</param>
        /// <param name="dto">New dto of the requested database record</param>
        void UpdateMusicianById(string id, MusicianDto dto);

        /// <summary>
        /// Update existing song record in the database with inputs from the console
        /// </summary>
        /// <param name="id">Id of the requested database record</param>
        /// <param name="dto">New dto of the requested database record</param>
        void UpdateSongById(string id, SongDto dto);

        /// <summary>
        /// Forwarder method for <see cref="IRepository.DeleteBandById"/>
        /// </summary>
        /// <param name="id">Id of the deletable database record</param>
        void DeleteBandById(string id);

        /// <summary>
        /// Forwarder method for <see cref="IRepository.DeleteAlbumById"/>
        /// </summary>
        /// <param name="id">Id of the deletable database record</param>
        void DeleteAlbumById(string id);

        /// <summary>
        /// Forwarder method for <see cref="IRepository.DeleteLabelById"/>
        /// </summary>
        /// <param name="id">Id of the deletable database record</param>
        void DeleteLabelById(string id);

        /// <summary>
        /// Forwarder method for <see cref="IRepository.DeleteMusicianById"/>
        /// </summary>
        /// <param name="id">Id of the deletable database record</param>
        void DeleteMusicianById(string id);

        /// <summary>
        /// Forwarder method for <see cref="IRepository.DeleteSongById"/>
        /// </summary>
        /// <param name="id">Id of the deletable database record</param>
        void DeleteSongById(string id);

        /// <summary>
        /// List all records in the album table matching with the band
        /// </summary>
        /// <param name="bandName">Value for filtering</param>
        /// <returns>List of filtered albums</returns>
        List<AlbumDto> ListAllAlbumsByBand(string bandName);

        /// <summary>
        /// List all records in the song table matching with the album
        /// </summary>
        /// <param name="albumName">Value for filtering</param>
        /// <returns>List of filtered songs</returns>
        List<SongDto> ListAllSongsByAlbum(string albumName);

        /// <summary>
        /// List all records in the song table matching with the album
        /// </summary>
        /// <param name="bandName">Value for filtering</param>
        /// <returns>List of filtered songs</returns>
        List<SongDto> ListAllSongsByBand(string bandName);

        /// <summary>
        /// List all records in the band table matching with the album
        /// </summary>
        /// <param name="genre">Value for filtering</param>
        /// <returns>List of filtered bands</returns>
        List<BandDto> ListBandsByGenre(string genre);

        /// <summary>
        /// List all records in the musician table matching with the album
        /// </summary>
        /// <param name="bandName">Value for filtering</param>
        /// <returns>List of filtered musicians</returns>
        List<MusicianDto> ListMusiciansByBand(string bandName);

        /// <summary>
        /// List all records in the musician table matching with the album
        /// </summary>
        /// <param name="albumName">Value for filtering</param>
        /// <returns>List of filtered musicians</returns>
        List<MusicianDto> ListMusiciansPerformedOnAlbum(string albumName);

        /// <summary>
        /// List all records in the musician table matching with the album
        /// </summary>
        /// <param name="labelName">Value for filtering</param>
        /// <returns>List of filtered musicians</returns>
        List<MusicianDto> ListMusiciansByReleasingUnderLabel(string labelName);

        /// <summary>
        /// List all records in the membership table matching with the album
        /// </summary>
        /// <param name="musicianName">Value for filtering</param>
        /// <returns>List of filtered memberships</returns>
        List<MembershipDto> ListMembershipsByMusician(string musicianName);

        /// <summary>
        /// List all records in the membership table matching with the album
        /// </summary>
        /// <param name="bandName">Value for filtering</param>
        /// <returns>List of filtered memberships</returns>
        List<MembershipDto> ListMembershipsByBand(string bandName);

        /// <summary>
        /// Forwarder method for <see cref="IRepository.GetBandById"/>
        /// </summary>
        /// <param name="selectedId">Id of the requested database record</param>
        /// <returns>Data transfer object of the request database record</returns>
        BandDto GetBandById(string selectedId);

        /// <summary>
        /// Forwarder method for <see cref="IRepository.GetLabelById"/>
        /// </summary>
        /// <param name="selectedId">Id of the requested database record</param>
        /// <returns>Data transfer object of the request database record</returns>
        LabelDto GetLabelById(string selectedId);

        /// <summary>
        /// Forwarder method for <see cref="IRepository.GetAlbumById"/>
        /// </summary>
        /// <param name="selectedId">Id of the requested database record</param>
        /// <returns>Data transfer object of the request database record</returns>
        AlbumDto GetAlbumById(string selectedId);

        /// <summary>
        /// Forwarder method for <see cref="IRepository.GetMusicianById"/>
        /// </summary>
        /// <param name="selectedId">Id of the requested database record</param>
        /// <returns>Data transfer object of the request database record</returns>
        MusicianDto GetMusicianById(string selectedId);

        /// <summary>
        /// Forwarder method for <see cref="IRepository.GetSongById"/>
        /// </summary>
        /// <param name="selectedId">Id of the requested database record</param>
        /// <returns>Data transfer object of the request database record</returns>
        SongDto GetSongById(string selectedId);
    }
}
