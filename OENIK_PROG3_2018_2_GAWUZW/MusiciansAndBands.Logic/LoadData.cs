﻿// <copyright file="LoadData.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <summary>MusiciansAndBands.Logic</summary>

namespace MusiciansAndBands.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Xml.Linq;
    using MusiciansAndBands.Dtos;
    using MusiciansAndBands.Repository;

    /// <summary>
    /// Load data from xml
    /// </summary>
    public class LoadData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoadData"/> class.
        /// </summary>
        /// <param name="repository">Repository instance</param>
        public LoadData(IRepository repository)
        {
            Repository = repository;
        }

        /// <summary>
        /// Gets or sets repository instance
        /// </summary>
        /// <value>The Repository class instance</value>
        private IRepository Repository { get; set; }

        /// <summary>
        /// Load default data from xml
        /// </summary>
        public void Load()
        {
            LoadLabels();
            LoadBands();
            LoadAlbums();
            LoadSongs();
            LoadMusicians();
            LoadMemberships();
        }

        /// <summary>
        /// Load default data
        /// </summary>
        private void LoadAlbums()
        {
            XDocument xml = XDocument.Load("albums.xml");
            List<AlbumDto> list = xml.Descendants("album").Select(x => new AlbumDto()
            {
                Album_ID = int.Parse(x.Element("id").Value),
                Album_name = x.Element("name").Value,
                Year_Released = DateTime.ParseExact(x.Element("date").Value, "yyyy.mm.dd", CultureInfo.InvariantCulture),
                Label = x.Element("label").Value,
                BandId = x.Element("bandid").Value,
                Producer = x.Element("producer").Value
            }).ToList();

            foreach (AlbumDto item in list)
            {
                Repository.CreateNewAlbum(item);
            }
        }

        /// <summary>
        /// Load default data
        /// </summary>
        private void LoadBands()
        {
            XDocument xml = XDocument.Load("bands.xml");

            List<BandDto> list = xml.Descendants("band").Select(x => new BandDto()
            {
                Band_ID = int.Parse(x.Element("id").Value),
                Band_name = x.Element("name").Value,
                Form_date = DateTime.ParseExact(x.Element("date").Value, "yyyy.mm.dd", CultureInfo.InvariantCulture),
                Hometown = x.Element("hometown").Value,
                Label = x.Element("label").Value
            }).ToList();

            foreach (BandDto item in list)
            {
                Repository.CreateNewBand(item);
            }
        }

        /// <summary>
        /// Load default data
        /// </summary>
        private void LoadLabels()
        {
            XDocument xml = XDocument.Load("labels.xml");

            List<LabelDto> list = xml.Descendants("label").Select(x => new LabelDto()
            {
                Label_name = x.Element("name").Value,
                Form_date = DateTime.ParseExact(x.Element("date").Value, "yyyy.mm.dd", CultureInfo.InvariantCulture)
            }).ToList();

            foreach (LabelDto item in list)
            {
                Repository.CreateNewLabel(item);
            }
        }

        /// <summary>
        /// Load default data
        /// </summary>
        private void LoadMemberships()
        {
            XDocument xml = XDocument.Load("memberships.xml");

            List<MembershipDto> list = xml.Descendants("membership").Select(x => new MembershipDto()
            {
                Membership_ID = int.Parse(x.Element("id").Value),
                Position = x.Element("instrument").Value,
                Musician_name = x.Element("name").Value,
                Date_of_birth = DateTime.ParseExact(x.Element("birthdate").Value, "yyyy.mm.dd", CultureInfo.InvariantCulture),
                Band_ID = int.Parse(x.Element("bandid").Value)
            }).ToList();

            foreach (MembershipDto item in list)
            {
                Repository.CreateNewMembership(item);
            }
        }

        /// <summary>
        /// Load default data
        /// </summary>
        private void LoadMusicians()
        {
            XDocument xml = XDocument.Load("musicians.xml");

            List<MusicianDto> list = xml.Descendants("musician").Select(x => new MusicianDto()
            {
                Musician_name = x.Element("name").Value,
                Date_of_birth = DateTime.ParseExact(x.Element("birthdate").Value, "yyyy.mm.dd", CultureInfo.InvariantCulture),
                Hometown = x.Element("hometown").Value
            }).ToList();

            foreach (MusicianDto item in list)
            {
                Repository.CreateNewMusician(item);
            }
        }

        /// <summary>
        /// Load default data
        /// </summary>
        private void LoadSongs()
        {
            XDocument xml = XDocument.Load("songs.xml");

            List<SongDto> list = xml.Descendants("song").Select(x => new SongDto()
            {
                Song_ID = int.Parse(x.Element("id").Value),
                Title = x.Element("name").Value,
                Genre = x.Element("genre").Value,
                Album_ID = int.Parse(x.Element("albumid").Value)
            }).ToList();

            foreach (SongDto item in list)
            {
                Repository.CreateNewSong(item);
            }
        }
    }
}
