﻿// <copyright file="LogicImpl.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <summary>This is the Logic.</summary>
namespace MusiciansAndBands.Logic
{
    using System.Collections.Generic;
    using System.Linq;
    using MusiciansAndBands.Dtos;
    using MusiciansAndBands.Repository;
    using MusiciansAndBands.Shared;

    /// <summary>
    /// Implementation of <see cref="ILogic"/>
    /// </summary>
    public class LogicImpl : ILogic
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LogicImpl"/> class.
        /// </summary>
        public LogicImpl()
        {
            Repository = new MusiciansAndBands.Repository.RepositoryImpl();
            LoadData loadData = new LoadData(Repository);
            loadData.Load();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LogicImpl"/> class.
        /// </summary>
        /// <param name="repository">Existing instance</param>
        public LogicImpl(IRepository repository)
        {
            Repository = repository;
        }

        /// <summary>
        /// Gets or sets repository instance
        /// </summary>
        /// /// <value>The Repository class instance</value>
        private IRepository Repository { get; set; }

        /**************************/
        /*
         * CREATE
        /*
        /**************************/

        /// <inheritdoc/>
        public void AddNewBand(BandDto dto)
        {
            Repository.CreateNewBand(dto);
        }

        /// <inheritdoc/>
        public void AddNewLabel(LabelDto dto)
        {
            Repository.CreateNewLabel(dto);
        }

        /// <inheritdoc/>
        public void AddNewAlbum(AlbumDto dto)
        {
            Repository.CreateNewAlbum(dto);
        }

        /// <inheritdoc/>
        public void AddNewMusician(MusicianDto dto)
        {
            Repository.CreateNewMusician(dto);
        }

        /// <inheritdoc/>
        public void AddNewSong(SongDto dto)
        {
            Repository.CreateNewSong(dto);
        }

        /**************************/
        /*
         * LIST
        /*
        /**************************/

        /// <inheritdoc/>
        public List<BandDto> ListAllBands()
        {
            return Repository.ListAllBands();
        }

        /// <inheritdoc/>
        public List<AlbumDto> ListAllAlbums()
        {
            return Repository.ListAllAlbums();
        }

        /// <inheritdoc/>
        public List<LabelDto> ListAllLabels()
        {
            return Repository.ListAllLabels();
        }

        /// <inheritdoc/>
        public List<MusicianDto> ListAllMusicians()
        {
            return Repository.ListAllMusicians();
        }

        /// <inheritdoc/>
        public List<SongDto> ListAllSongs()
        {
            return Repository.ListAllSongs();
        }

        /**************************/
        /*
         * GET
        /*
        /**************************/

        /// <inheritdoc/>
        public BandDto GetBandById(string selectedId)
        {
            return Repository.GetBandById(int.Parse(selectedId));
        }

        /// <inheritdoc/>
        public LabelDto GetLabelById(string selectedId)
        {
            return Repository.GetLabelById(selectedId);
        }

        /// <inheritdoc/>
        public AlbumDto GetAlbumById(string selectedId)
        {
            return Repository.GetAlbumById(int.Parse(selectedId));
        }

        /// <inheritdoc/>
        public MusicianDto GetMusicianById(string selectedId)
        {
            return Repository.GetMusicianById(selectedId);
        }

        /// <inheritdoc/>
        public SongDto GetSongById(string selectedId)
        {
            return Repository.GetSongById(int.Parse(selectedId));
        }

        /**************************/
        /*
         * UPDATE
        /*
        /**************************/

        /// <inheritdoc/>
        public void UpdateBandById(string id, BandDto dto)
        {
            Repository.UpdateBandById(int.Parse(id), dto);
        }

        /// <inheritdoc/>
        public void UpdateAlbumById(string id, AlbumDto dto)
        {
            Repository.UpdateAlbumById(int.Parse(id), dto);
        }

        /// <inheritdoc/>
        public void UpdateLabelById(string id, LabelDto dto)
        {
            Repository.UpdateLabelById(id, dto);
        }

        /// <inheritdoc/>
        public void UpdateMusicianById(string id, MusicianDto dto)
        {
            Repository.UpdateMusicianById(id, dto);
        }

        /// <inheritdoc/>
        public void UpdateSongById(string id, SongDto dto)
        {
            Repository.UpdateSongById(int.Parse(id), dto);
        }

        /**************************/
        /*
         * DELETE
        /*
        /**************************/

        /// <inheritdoc/>
        public void DeleteBandById(string id)
        {
            RemoveMembershipsOnBandDelete(id);
            Repository.DeleteBandById(int.Parse(id));
        }

        /// <inheritdoc/>
        public void DeleteAlbumById(string id)
        {
            Repository.DeleteAlbumById(int.Parse(id));
        }

        /// <inheritdoc/>
        public void DeleteLabelById(string id)
        {
            Repository.DeleteLabelById(id);
        }

        /// <inheritdoc/>
        public void DeleteMusicianById(string id)
        {
            RemoveMembershipsOnMusicianDelete(id);
            Repository.DeleteMusicianById(id);
        }

        /// <inheritdoc/>
        public void DeleteSongById(string id)
        {
            Repository.DeleteSongById(int.Parse(id));
        }

        /**************************/
        /*
         * SPECIAL REQUESTS
        /*
        /**************************/

        /// <inheritdoc/>
        public List<AlbumDto> ListAllAlbumsByBand(string bandName)
        {
            var bandExist = (from band in Repository.ListAllBands()
                             where band.Band_name == bandName
                             select band).ToList();

            if (bandExist.Count() == 0)
            {
                throw new BandException("There is no band with this name.");
            }

            var allAlbums = Repository.ListAllAlbums();
            var filteredAlbums = (from album in allAlbums
                                  where album.BandName == bandName
                                  select album).ToList();

            if (filteredAlbums.Count() == 0)
            {
                throw new AlbumException("There is no album from this band.");
            }

            return filteredAlbums;
        }

        /// <inheritdoc/>
        public List<SongDto> ListAllSongsByAlbum(string albumName)
        {
            bool albumExist = (from album in Repository.ListAllAlbums()
                               where album.Album_name == albumName
                               select album).ToList().Any();
            if (!albumExist)
            {
                throw new AlbumException("There is no album with this name.");
            }

            var allSongs = Repository.ListAllSongs();
            var filteredSongs = (from song in allSongs
                                 where song.Album == albumName
                                 select song).ToList();

            if (filteredSongs.Count() == 0)
            {
                throw new SongException("There is no song from this album.");
            }

            return filteredSongs;
        }

        /// <inheritdoc/>
        public List<SongDto> ListAllSongsByBand(string bandName)
        {
            var bandExist = (from band in Repository.ListAllBands()
                             where band.Band_name == bandName
                             select band).ToList();

            if (bandExist.Count() == 0)
            {
                throw new BandException("There is no band with this name.");
            }

            var allSongs = Repository.ListAllSongs();
            var filteredSongs = (from song in allSongs
                                 where song.Band == bandName
                                 select song).ToList();

            if (filteredSongs.Count() == 0)
            {
                throw new SongException("There is no song from this band.");
            }

            return filteredSongs;
        }

        /// <inheritdoc/>
        public List<BandDto> ListBandsByGenre(string genre)
        {
            List<BandDto> returnList = new List<BandDto>();

            var songsGroupByBands = from song in Repository.ListAllSongs()
                                    group song by song.Band into songs
                                    select songs;

            foreach (var songGroup in songsGroupByBands)
            {
                foreach (SongDto song in songGroup)
                {
                    if (song.Genre == genre)
                    {
                        returnList.AddRange((from band in ListAllBands()
                                        where band.Band_name == song.Band
                                        select band).ToList());
                        break;
                    }
                }
            }

            if (returnList.Count() == 0)
            {
                throw new GenreException("There is no band in this genre.");
            }

            return returnList;
        }

        /// <inheritdoc/>
        public List<MusicianDto> ListMusiciansByBand(string bandName)
        {
            var bandExist = (from band in Repository.ListAllBands()
                             where band.Band_name == bandName
                             select band).ToList();

            if (bandExist.Count() == 0)
            {
                throw new BandException("There is no band with this name.");
            }

            List<MusicianDto> returnList = new List<MusicianDto>();

            var musicians = from membership in Repository.ListAllMemberships()
                            where membership.Band == bandName
                            select membership.Musician_name;

            foreach (string item in musicians)
            {
                returnList.Add((from musician in Repository.ListAllMusicians()
                                where musician.Musician_name == item
                                select musician).Single());
            }

            if (returnList.Count() == 0)
            {
                throw new MusicianException("There is no members in this band.");
            }

            return returnList;
        }

        /// <inheritdoc/>
        public List<MusicianDto> ListMusiciansPerformedOnAlbum(string albumName)
        {
            var bandName = (from album in Repository.ListAllAlbums()
                            where album.Album_name == albumName
                            select album.BandName).Single();

            if (bandName.Count() == 0)
            {
                throw new BandException("There is no band with this name.");
            }

            return ListMusiciansByBand(bandName);
        }

        /// <inheritdoc/>
        public List<MusicianDto> ListMusiciansByReleasingUnderLabel(string labelName)
        {
            List<MusicianDto> returnList = new List<MusicianDto>();
            List<BandDto> bandList = new List<BandDto>();

            var bandNamesReleasedUnderLabel = (from album in Repository.ListAllAlbums()
                                               where album.Label == labelName
                                               select album.BandName).ToList();

            foreach (string bandName in bandNamesReleasedUnderLabel)
            {
                bandList.Add(Repository.ListAllBands().Where(band => band.Band_name == bandName &&
                                                                            !bandList.Contains(band)).FirstOrDefault());
            }

            foreach (BandDto item in bandList)
            {
                returnList.AddRange(ListMusiciansByBand(item.Band_name));
            }

            return returnList;
        }

        /// <inheritdoc/>
        public List<MembershipDto> ListMembershipsByMusician(string musicianName)
        {
            var memberships = (from membership in Repository.ListAllMemberships()
                               where membership.Musician_name == musicianName
                               select membership).ToList();

            return memberships;
        }

        /// <inheritdoc/>
        public List<MembershipDto> ListMembershipsByBand(string bandName)
        {
            var memberships = (from membership in Repository.ListAllMemberships()
                               where membership.Band == bandName
                               select membership).ToList();

            return memberships;
        }

        private void RemoveMembershipsOnMusicianDelete(string id)
        {
            var musician = Repository.GetMusicianById(id);
            List<MembershipDto> memberships = ListMembershipsByMusician(musician.Musician_name);

            foreach (MembershipDto item in memberships)
            {
                Repository.DeleteMembershipById(item.Membership_ID);
            }
        }

        private void RemoveMembershipsOnBandDelete(string id)
        {
            var band = Repository.GetBandById(int.Parse(id));
            List<MembershipDto> memberships = ListMembershipsByBand(band.Band_name);

            foreach (MembershipDto item in memberships)
            {
                Repository.DeleteMembershipById(item.Membership_ID);
            }
        }
    }
}
