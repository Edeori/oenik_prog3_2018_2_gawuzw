﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <summary>This is the Top Level.</summary>
namespace MusiciansAndBands.Program
{
    using System;

    /// <summary>
    /// Entry point class
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Entry point
        /// </summary>
        /// <param name="args">What?</param>
        public static void Main(string[] args)
        {
            Menu menu = new Menu();
            bool exitable = false;
            while (exitable == false)
            {
                menu.WriteMenu();
                exitable = menu.SelectMenuItem(Console.ReadLine());
                /*/
                try
                {
                    exitable = menu.SelectMenuItem(Console.ReadLine());
                }
                catch (InvalidOperationException)
                {
                    Console.Clear();
                    Console.WriteLine("Invalid Operation!");
                    Console.WriteLine("Press any key...");
                    Console.ReadKey();
                }
                /*/
                Console.Clear();
            }
        }
    }
}
