﻿// <copyright file="Create.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <summary>This is the Logic.</summary>
namespace MusiciansAndBands.Logic
{
    using System;
    using System.Collections.Generic;
    using MusiciansAndBands.Shared;

    /// <summary>
    /// Methods for creating new records in the database.
    /// </summary>
    internal class Create
    {
        /// <summary>
        /// Values enabled on exit
        /// </summary>
        private static readonly List<string> EnableExit = new List<string> { "exit", "yes", "finished" };

        /// <summary>
        /// Initializes a new instance of the <see cref="Create"/> class.
        /// </summary>
        /// <param name="logic">Existing instance of logic</param>
        internal Create(ILogic logic)
        {
            Logic = logic;
            InputReader = new InputReader();
        }

        private ILogic Logic { get; set; }

        /// <summary>
        /// Gets or sets InputReader instance
        /// </summary>
        /// <value>The InputReader class instance</value>
        private InputReader InputReader { get; set; }

        /// <summary>
        /// Connect the InputReader output for the Repository to store new band
        /// </summary>
        internal void CreateBand()
        {
            Logic.AddNewBand(InputReader.InputBand(StringsToConsole.CreateBand));
        }

        /// <summary>
        /// Connect the InputReader output for the Repository to store new label
        /// </summary>
        internal void CreateLabel()
        {
            Logic.AddNewLabel(InputReader.InputLabel(StringsToConsole.CreateLabel));
        }

        /// <summary>
        /// Connect the InputReader output for the Repository to store new album
        /// </summary>
        internal void CreateAlbum()
        {
            Logic.AddNewAlbum(InputReader.InputAlbum(StringsToConsole.CreateAlbum));
        }

        /// <summary>
        /// Connect the InputReader output for the Repository to store new musician
        /// </summary>
        internal void CreateMusician()
        {
            Logic.AddNewMusician(InputReader.InputMusician(StringsToConsole.CreateMusician));
        }

        /// <summary>
        /// Connect the InputReader output for the Repository to store new song
        /// </summary>
        internal void CreateSong()
        {
            Logic.AddNewSong(InputReader.InputSong(StringsToConsole.CreateSong));
        }

        /// <summary>
        /// Connect the InputReader output for the Repository to store multiple bands
        /// </summary>
        internal void MultipleBandsCreate()
        {
            bool exitable = true;
            do
            {
                Logic.AddNewBand(InputReader.InputBand(StringsToConsole.CreateMultipleBands));

                Console.WriteLine("Have you finished?");
                string input = Console.ReadLine();
                exitable = !EnableExit.Contains(input.ToLower());
            }
            while (exitable);
        }

        /// <summary>
        /// Connect the InputReader output for the Repository to store multiple labels
        /// </summary>
        internal void MultipleLabelsCreate()
        {
            bool exitable = true;
            do
            {
                Logic.AddNewLabel(InputReader.InputLabel(StringsToConsole.CreateMultipleLabels));

                Console.WriteLine("Have you finished?");
                string input = Console.ReadLine();
                exitable = !EnableExit.Contains(input.ToLower());
            }
            while (exitable);
        }

        /// <summary>
        /// Connect the InputReader output for the Repository to store multiple albums
        /// </summary>
        internal void MultipleAlbumsCreate()
        {
            bool exitable = true;
            do
            {
                Logic.AddNewAlbum(InputReader.InputAlbum(StringsToConsole.CreateMultipleAlbums));

                Console.WriteLine("Have you finished?");
                string input = Console.ReadLine();
                exitable = !EnableExit.Contains(input.ToLower());
            }
            while (exitable);
        }

        /// <summary>
        /// Connect the InputReader output for the Repository to store multiple musicians
        /// </summary>
        internal void MultipleMusiciansCreate()
        {
            bool exitable = true;
            do
            {
                Logic.AddNewMusician(InputReader.InputMusician(StringsToConsole.CreateMultipleMusicians));

                Console.WriteLine("Have you finished?");
                string input = Console.ReadLine();
                exitable = !EnableExit.Contains(input.ToLower());
            }
            while (exitable);
        }

        /// <summary>
        /// Connect the InputReader output for the Repository to store multiple songs
        /// </summary>
        internal void MultipleSongsCreate()
        {
            bool exitable = true;
            do
            {
                Logic.AddNewSong(InputReader.InputSong(StringsToConsole.CreateMultipleSongs));

                Console.WriteLine("Have you finished?");
                string input = Console.ReadLine();
                exitable = !EnableExit.Contains(input.ToLower());
            }
            while (exitable);
        }
    }
}
