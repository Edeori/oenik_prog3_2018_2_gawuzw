﻿// <copyright file="Menu.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <summary>This is the Top Level.</summary>
namespace MusiciansAndBands.Program
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Xml.Linq;
    using MusiciansAndBands.Logic;
    using MusiciansAndBands.Shared;

    /// <summary>
    /// Menu class that display options on console
    /// </summary>
    public class Menu
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Menu"/> class.
        /// </summary>
        public Menu()
        {
            Logic = new MusiciansAndBands.Logic.LogicImpl();
            SpecialMenu = new SpecialMenuElements(Logic);
            InputReader = new InputReader();
            Create = new Create(Logic);
        }

        private ILogic Logic { get; set; }

        private SpecialMenuElements SpecialMenu { get; set; }

        private InputReader InputReader { get; set; }

        private Create Create { get; set; }

        /// <summary>
        /// Write menu messages to console
        /// </summary>
        public void WriteMenu()
        {
            Console.WriteLine(StringsToConsole.MenuText);
            Console.WriteLine(StringsToConsole.MenuKeyWords);
            Console.WriteLine(StringsToConsole.SpecialMenu);
        }

        /// <summary>
        /// Method that decides the called function depending on the input string
        /// </summary>
        /// <param name="selected">Input string from console</param>
        /// <returns>Exit from application on input string "Exit"</returns>
        public bool SelectMenuItem(string selected)
        {
            /*/
             * Checking the selection if the user want to list all the elements of a table in the database
            /*/

            if (selected.Contains("ListAll"))
            {
                string selectedTable = selected.Split('.')[1];
                switch (selectedTable)
                {
                    case "Bands":
                        CustomListExtensions.ToConsole(Logic.ListAllBands(), selectedTable);
                        break;
                    case "Labels":
                        CustomListExtensions.ToConsole(Logic.ListAllLabels(), selectedTable);
                        break;
                    case "Albums":
                        CustomListExtensions.ToConsole(Logic.ListAllAlbums(), selectedTable);
                        break;
                    case "Musicians":
                        CustomListExtensions.ToConsole(Logic.ListAllMusicians(), selectedTable);
                        break;
                    case "Songs":
                        CustomListExtensions.ToConsole(Logic.ListAllSongs(), selectedTable);
                        break;
                    default:
                        throw new InvalidOperationException();
                }
            }

            /*/
             * Checking the selection if the user want to get an element from a table by the element id
            /*/

            if (selected.Contains("Get"))
            {
                string selectedId = selected.Split('.')[1];
                string selectedTable = selected.Split('.')[2];

                switch (selectedTable)
                {
                    case "Bands":
                        CustomListExtensions.DtoToConsole(Logic.GetBandById(selectedId), selectedTable);
                        break;
                    case "Labels":
                        CustomListExtensions.DtoToConsole(Logic.GetLabelById(selectedId), selectedTable);
                        break;
                    case "Albums":
                        CustomListExtensions.DtoToConsole(Logic.GetAlbumById(selectedId), selectedTable);
                        break;
                    case "Musicians":
                        CustomListExtensions.DtoToConsole(Logic.GetMusicianById(selectedId), selectedTable);
                        break;
                    case "Songs":
                        CustomListExtensions.DtoToConsole(Logic.GetSongById(selectedId), selectedTable);
                        break;
                    default:
                        throw new InvalidOperationException();
                }
            }

            /*/
             * Checking the selection if the user want to create a new item in the database
            /*/

            if (selected.Contains("Create"))
            {
                string selectedTable = selected.Split('.')[1];

                if (selected.Split('.').Length > 2)
                {
                    bool isMultipleCreate = selected.Split('.')[2] == "Multiple";

                    /*/
                     * Check if the selection is a multi creation
                    /*/

                    if (isMultipleCreate)
                    {
                        switch (selectedTable)
                        {
                            case "Bands":
                                Create.MultipleBandsCreate();
                                break;
                            case "Labels":
                                Create.MultipleLabelsCreate();
                                break;
                            case "Albums":
                                Create.MultipleAlbumsCreate();
                                break;
                            case "Musicians":
                                Create.MultipleMusiciansCreate();
                                break;
                            case "Songs":
                                Create.MultipleSongsCreate();
                                break;
                            default:
                                throw new InvalidOperationException();
                        }
                    }
                }
                else
                {
                    switch (selectedTable)
                    {
                        case "Bands":
                            Create.CreateBand();
                            break;
                        case "Labels":
                            Create.CreateLabel();
                            break;
                        case "Albums":
                            Create.CreateAlbum();
                            break;
                        case "Musicians":
                            Create.CreateMusician();
                            break;
                        case "Songs":
                            Create.CreateSong();
                            break;
                        default:
                            throw new InvalidOperationException();
                    }
                }
            }

            if (selected.Contains("Update"))
            {
                string selectedId = selected.Split('.')[1];
                string selectedTable = selected.Split('.')[2];

                switch (selectedTable)
                {
                    case "Bands":
                        Logic.UpdateBandById(selectedId, InputReader.InputBand(StringsToConsole.UpdateBand));
                        break;
                    case "Labels":
                        Logic.UpdateLabelById(selectedId, InputReader.InputLabel(StringsToConsole.UpdateLabel));
                        break;
                    case "Albums":
                        Logic.UpdateAlbumById(selectedId, InputReader.InputAlbum(StringsToConsole.UpdateAlbum));
                        break;
                    case "Musicians":
                        Logic.UpdateMusicianById(selectedId, InputReader.InputMusician(StringsToConsole.UpdateMusician));
                        break;
                    case "Songs":
                        Logic.UpdateSongById(selectedId, InputReader.InputSong(StringsToConsole.UpdateSong));
                        break;
                    default:
                        throw new InvalidOperationException();
                }
            }

            if (selected.Contains("Delete"))
            {
                string selectedId = selected.Split('.')[1];
                string selectedTable = selected.Split('.')[2];

                switch (selectedTable)
                {
                    case "Bands":
                        Logic.DeleteBandById(selectedId);
                        break;
                    case "Labels":
                        Logic.DeleteLabelById(selectedId);
                        break;
                    case "Albums":
                        Logic.DeleteAlbumById(selectedId);
                        break;
                    case "Musicians":
                        Logic.DeleteMusicianById(selectedId);
                        break;
                    case "Songs":
                        Logic.DeleteSongById(selectedId);
                        break;
                    default:
                        throw new InvalidOperationException();
                }
            }

            if (selected.Contains("spec"))
            {
                string selectedMenu = selected.Split('.')[1];

                switch (selectedMenu)
                {
                    case "SongsByAlbum":
                        SpecialMenu.SongsByAlbum();
                        break;
                    case "SongsByBand":
                        SpecialMenu.SongsByBand();
                        break;
                    case "BandsByGenre":
                        SpecialMenu.BandsByGenre();
                        break;
                    case "MusiciansByBand":
                        SpecialMenu.MusiciansByBand();
                        break;
                    case "MusiciansOnAlbum":
                        SpecialMenu.MusiciansOnAlbum();
                        break;
                    case "MusiciansUnderLabel":
                        SpecialMenu.MusiciansUnderLabel();
                        break;
                    default:
                        throw new InvalidOperationException();
                }
            }

            if (selected.Contains("Facebook"))
            {
                string selectedMenu = selected.Split('.')[1];

                string facebook = new WebClient().DownloadString("http://localhost:8080/BandServlet/Facebook?band=" + selectedMenu);

                Console.Clear();
                Console.WriteLine(facebook);
                Console.WriteLine("Press any key...");
                Console.ReadKey();
            }

            return selected == "Exit";
        }
    }
}
