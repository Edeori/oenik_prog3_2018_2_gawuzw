﻿// <copyright file="CustomListExtensions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <summary>This is the Top Level.</summary>
namespace MusiciansAndBands.Program
{
    using System;
    using System.Collections.Generic;
    using MusiciansAndBands.Dtos;

    /// <summary>
    /// Static class for simplify the write to console
    /// </summary>
    public static class CustomListExtensions
    {
        /// <summary>
        /// Write multiple lines to console
        /// </summary>
        /// <typeparam name="T">Generic</typeparam>
        /// <param name="input">Input data to write</param>
        /// <param name="header">Some header</param>
        public static void ToConsole<T>(this IEnumerable<T> input, string header)
        {
            Console.WriteLine($"************ {header} ************");
            foreach (var item in input)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine($"************ {header} ************");
            Console.WriteLine("\nPress any key...");
            Console.ReadLine();
        }

        /// <summary>
        /// Write data transfer object details to console
        /// </summary>
        /// <param name="inputDto">Data transfer object</param>
        /// <param name="header">Some header</param>
        public static void DtoToConsole(this BaseDto inputDto, string header)
        {
            Console.WriteLine($"************ {header} ************");
            Console.WriteLine(inputDto);
            Console.WriteLine($"************ {header} ************");
            Console.WriteLine("\nPress any key...");
            Console.ReadLine();
        }
    }
}
