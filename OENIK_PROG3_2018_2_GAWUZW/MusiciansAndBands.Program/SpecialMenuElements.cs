﻿// <copyright file="SpecialMenuElements.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <summary>This is the Top Level.</summary>
namespace MusiciansAndBands.Program
{
    using System;
    using MusiciansAndBands.Logic;
    using MusiciansAndBands.Shared;

    /// <summary>
    /// Special Menu element caller class
    /// </summary>
    public class SpecialMenuElements
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SpecialMenuElements"/> class.
        /// </summary>
        /// <param name="logic">Logic instance</param>
        public SpecialMenuElements(ILogic logic)
        {
            Logic = logic;
        }

        private ILogic Logic { get; set; }

        /// <summary>
        /// Menu item - call the logic with for specific filter with input parameters
        /// </summary>
        public void SongsByAlbum()
        {
            Console.Clear();
            StringsToConsole.SpecialMenuElements("Songs", "Album", "Album Name");
            try
            {
                CustomListExtensions.ToConsole(Logic.ListAllSongsByAlbum(Console.ReadLine()), StringsToConsole.SongsByAlbum);
            }
            catch (AlbumException ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }
            catch (SongException ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }
        }

        /// <summary>
        /// Menu item - call the logic with for specific filter with input parameters
        /// </summary>
        public void SongsByBand()
        {
            Console.Clear();
            StringsToConsole.SpecialMenuElements("Songs", "Band", "Band Name");
            try
            {
                CustomListExtensions.ToConsole(Logic.ListAllSongsByBand(Console.ReadLine()), StringsToConsole.SongsByBand);
            }
            catch (BandException ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }
        }

        /// <summary>
        /// Menu item - call the logic with for specific filter with input parameters
        /// </summary>
        public void BandsByGenre()
        {
            Console.Clear();
            StringsToConsole.SpecialMenuElements("Bands", "Genre", "Genre");
            try
            {
                CustomListExtensions.ToConsole(Logic.ListBandsByGenre(Console.ReadLine()), StringsToConsole.BandsByGenre);
            }
            catch (GenreException ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }
        }

        /// <summary>
        /// Menu item - call the logic with for specific filter with input parameters
        /// </summary>
        public void MusiciansByBand()
        {
            Console.Clear();
            StringsToConsole.SpecialMenuElements("Musicians", "Band", "Band");
            try
            {
                CustomListExtensions.ToConsole(Logic.ListMusiciansByBand(Console.ReadLine()), StringsToConsole.MusiciansByBand);
            }
            catch (BandException ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }
            catch (MusicianException ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }
        }

        /// <summary>
        /// Menu item - call the logic with for specific filter with input parameters
        /// </summary>
        public void MusiciansOnAlbum()
        {
            Console.Clear();
            StringsToConsole.SpecialMenuElements("Musicians", "Album", "Album");
            try
            {
                CustomListExtensions.ToConsole(Logic.ListMusiciansPerformedOnAlbum(Console.ReadLine()), StringsToConsole.MusiciansOnAlbum);
            }
            catch (BandException ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }
        }

        /// <summary>
        /// Menu item - call the logic with for specific filter with input parameters
        /// </summary>
        public void MusiciansUnderLabel()
        {
            Console.Clear();
            StringsToConsole.SpecialMenuElements("Musicians", "Label", "Label");
            {
                CustomListExtensions.ToConsole(Logic.ListMusiciansByReleasingUnderLabel(Console.ReadLine()), StringsToConsole.MusiciansUnderLabel);
            }
        }
    }
}
