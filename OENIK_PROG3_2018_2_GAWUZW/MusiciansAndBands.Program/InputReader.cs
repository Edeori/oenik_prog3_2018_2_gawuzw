﻿// <copyright file="InputReader.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <summary>This is the Logic.</summary>
namespace MusiciansAndBands.Logic
{
    using System;
    using System.Globalization;
    using MusiciansAndBands.Dtos;
    using MusiciansAndBands.Shared;

    /// <summary>
    /// Class for creating data transfer objects from the user input
    /// </summary>
    public class InputReader
    {
        /// <summary>
        /// Read from console the required values for band data transfer object
        /// </summary>
        /// <param name="message">Messages to console</param>
        /// <returns>Data transfer object</returns>
        public BandDto InputBand(string message)
        {
            Console.WriteLine(message);
            string[] details = new string[StringsToConsole.BandDetails.Length];
            BandDto dto = new BandDto();

            for (int i = 0; i < StringsToConsole.BandDetails.Length; i++)
            {
                Console.WriteLine(StringsToConsole.BandDetails[i]);
                details[i] = Console.ReadLine();
            }

            dto.Band_name = details[0];
            dto.Form_date = DateTime.ParseExact(details[1], "yyyy", CultureInfo.InvariantCulture);
            dto.Label = details[2];
            dto.Hometown = details[3];

            return dto;
        }

        /// <summary>
        /// Read from console the required values for label data transfer object
        /// </summary>
        /// <param name="message">Messages to console</param>
        /// <returns>Data transfer object</returns>
        public LabelDto InputLabel(string message)
        {
            Console.WriteLine(message);
            string[] details = new string[StringsToConsole.LabelDetails.Length];
            LabelDto dto = new LabelDto();

            for (int i = 0; i < StringsToConsole.LabelDetails.Length; i++)
            {
                Console.WriteLine(StringsToConsole.LabelDetails[i]);
                details[i] = Console.ReadLine();
            }

            dto.Label_name = details[0];
            dto.Form_date = DateTime.ParseExact(details[1], "yyyy", CultureInfo.InvariantCulture);

            return dto;
        }

        /// <summary>
        /// Read from console the required values for album data transfer object
        /// </summary>
        /// <param name="message">Messages to console</param>
        /// <returns>Data transfer object</returns>
        public AlbumDto InputAlbum(string message)
        {
            Console.WriteLine(message);
            string[] details = new string[StringsToConsole.AlbumDeatails.Length];
            AlbumDto dto = new AlbumDto();

            for (int i = 0; i < StringsToConsole.AlbumDeatails.Length; i++)
            {
                Console.WriteLine(StringsToConsole.AlbumDeatails[i]);
                details[i] = Console.ReadLine();
            }

            dto.Album_name = details[0];
            dto.Year_Released = DateTime.ParseExact(details[1], "yyyy", CultureInfo.InvariantCulture);
            dto.Label = details[2];
            dto.Producer = details[3];
            dto.BandName = details[4];

            return dto;
        }

        /// <summary>
        /// Read from console the required values for musician data transfer object
        /// </summary>
        /// <param name="message">Messages to console</param>
        /// <returns>Data transfer object</returns>
        public MusicianDto InputMusician(string message)
        {
            Console.WriteLine(message);
            string[] details = new string[StringsToConsole.MusicianDetails.Length];
            MusicianDto dto = new MusicianDto();

            for (int i = 0; i < StringsToConsole.MusicianDetails.Length; i++)
            {
                Console.WriteLine(StringsToConsole.MusicianDetails[i]);
                details[i] = Console.ReadLine();
            }

            dto.Musician_name = details[0];
            dto.Date_of_birth = DateTime.ParseExact(details[1], "yyyy.mm.dd.", CultureInfo.InvariantCulture);
            dto.Hometown = details[2];

            return dto;
        }

        /// <summary>
        /// Read from console the required values for song data transfer object
        /// </summary>
        /// <param name="message">Messages to console</param>
        /// <returns>Data transfer object</returns>
        public SongDto InputSong(string message)
        {
            Console.WriteLine(message);
            string[] details = new string[StringsToConsole.SongDetails.Length];
            SongDto dto = new SongDto();

            for (int i = 0; i < StringsToConsole.SongDetails.Length; i++)
            {
                Console.WriteLine(StringsToConsole.SongDetails[i]);
                details[i] = Console.ReadLine();
            }

            dto.Band = details[0];
            dto.Title = details[1];
            dto.Album = details[2];
            dto.Genre = details[3];

            return dto;
        }
    }
}
