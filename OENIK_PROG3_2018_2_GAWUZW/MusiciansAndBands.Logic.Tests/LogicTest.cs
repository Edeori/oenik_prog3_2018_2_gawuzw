﻿// <copyright file="LogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
// <summary>MusiciansAndBands.Logic.Tests</summary>

namespace MusiciansAndBands.Logic.Tests
{
    using System.Collections.Generic;
    using System.Linq;
    using Moq;
    using MusiciansAndBands.Dtos;
    using MusiciansAndBands.Repository;
    using MusiciansAndBands.Shared;
    using NUnit.Framework;

    /// <summary>
    /// Logic Test
    /// </summary>
    [TestFixture]
    public class LogicTest
    {
        private Mock<IRepository> MockRepository { get; set; }

        private LogicImpl Logic { get; set; }

        /// <summary>
        /// Set Up
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            MockRepository = new Mock<IRepository>();
            MockRepository.Setup(m => m.ListAllBands()).Returns(new List<BandDto> { });
            MockRepository.Setup(m => m.ListAllAlbums()).Returns(new List<AlbumDto> { });
            MockRepository.Setup(m => m.ListAllSongs()).Returns(new List<SongDto> { });
            MockRepository.Setup(m => m.ListAllLabels()).Returns(new List<LabelDto> { });
            MockRepository.Setup(m => m.ListAllMusicians()).Returns(new List<MusicianDto> { });
            MockRepository.Setup(m => m.ListAllMemberships()).Returns(new List<MembershipDto> { });
        }

        /// <summary>
        /// WhenListAllAlbumsByBand ThenReturnCorrectAlbums
        /// </summary>
        [Test]
        public void WhenListAllAlbumsByBand_ThenReturnCorrectAlbums()
        {
            // Arr
            BandDto bandDto = new BandDto() { Band_name = "a" };
            AlbumDto a = new AlbumDto() { BandName = "a", Album_name = "a_album" };
            AlbumDto b = new AlbumDto() { BandName = "a", Album_name = "b_album" };
            AlbumDto c = new AlbumDto() { BandName = "b", Album_name = "c_album" };
            MockRepository.Setup(m => m.ListAllBands()).Returns(new List<BandDto> { bandDto });
            MockRepository.Setup(m => m.ListAllAlbums()).Returns(new List<AlbumDto> { a, b, c });

            Logic = new LogicImpl(MockRepository.Object);

            // Act
            var result = Logic.ListAllAlbumsByBand("a");

            // Assert
            Assert.That(result.Count(), Is.EqualTo(2));
            Assert.That(result.Contains(a) && result.Contains(b));
        }

        /// <summary>
        /// WhenListAllAlbumsByBand AndInvalidParameter ThenThrowsException
        /// </summary>
        [Test]
        public void WhenListAllAlbumsByBand_AndInvalidParameter_ThenThrowsException()
        {
            // Arr
            Logic = new LogicImpl(MockRepository.Object);

            // Act
            var ex = Assert.Throws<BandException>(() => Logic.ListAllAlbumsByBand("c"));

            // Assert
            Assert.That(ex.Message, Is.EqualTo("There is no band with this name."));
        }

        /// <summary>
        /// WhenListAllAlbumsByBand AndNoAlbumInDb ThenThrowsException
        /// </summary>
        [Test]
        public void WhenListAllAlbumsByBand_AndNoAlbumInDb_ThenThrowsException()
        {
            // Arr
            BandDto bandDto = new BandDto() { Band_name = "c" };
            MockRepository.Setup(m => m.ListAllBands()).Returns(new List<BandDto> { bandDto });
            Logic = new LogicImpl(MockRepository.Object);

            // Act
            var ex = Assert.Throws<AlbumException>(() => Logic.ListAllAlbumsByBand("c"));

            // Assert
            Assert.That(ex.Message, Is.EqualTo("There is no album from this band."));
        }

        /// <summary>
        /// WhenListAllSongsByAlbum ThenReturnCorrectSongs
        /// </summary>
        [Test]
        public void WhenListAllSongsByAlbum_ThenReturnCorrectSongs()
        {
            // Arr
            AlbumDto albumDto = new AlbumDto() { Album_name = "A" };
            SongDto a = new SongDto() { Title = "a", Album = "A" };
            SongDto b = new SongDto() { Title = "b", Album = "A" };
            SongDto c = new SongDto() { Title = "c", Album = "B" };
            MockRepository.Setup(m => m.ListAllAlbums()).Returns(new List<AlbumDto> { albumDto });
            MockRepository.Setup(m => m.ListAllSongs()).Returns(new List<SongDto> { a, b, c });
            Logic = new LogicImpl(MockRepository.Object);

            // Act
            var result = Logic.ListAllSongsByAlbum("A");

            // Assert
            Assert.That(result.Count(), Is.EqualTo(2));
            Assert.That(result.Contains(a) && result.Contains(b));
        }

        /// <summary>
        /// WhenListAllSongsByAlbum AndInvalidParameter ThenThrowsException
        /// </summary>
        [Test]
        public void WhenListAllSongsByAlbum_AndInvalidParameter_ThenThrowsException()
        {
            // Arr
            SongDto a = new SongDto() { Title = "a", Album = "A" };
            SongDto b = new SongDto() { Title = "b", Album = "A" };
            SongDto c = new SongDto() { Title = "c", Album = "B" };
            MockRepository.Setup(m => m.ListAllSongs()).Returns(new List<SongDto> { a, b, c });
            Logic = new LogicImpl(MockRepository.Object);

            // Act
            var ex = Assert.Throws<AlbumException>(() => Logic.ListAllSongsByAlbum("C"));

            // Assert
            Assert.That(ex.Message, Is.EqualTo("There is no album with this name."));
        }

        /// <summary>
        /// WhenListAllSongsByAlbum AndNoSongInDb ThenThrowsException
        /// </summary>
        [Test]
        public void WhenListAllSongsByAlbum_AndNoSongInDb_ThenThrowsException()
        {
            // Arr
            AlbumDto albumDto = new AlbumDto() { Album_name = "A" };
            MockRepository.Setup(m => m.ListAllAlbums()).Returns(new List<AlbumDto> { albumDto });
            Logic = new LogicImpl(MockRepository.Object);

            // Act
            var ex = Assert.Throws<SongException>(() => Logic.ListAllSongsByAlbum("A"));

            // Assert
            Assert.That(ex.Message, Is.EqualTo("There is no song from this album."));
        }

        /// <summary>
        /// WhenListAllSongsByBand ThenReturnCorrectSongs
        /// </summary>
        [Test]
        public void WhenListAllSongsByBand_ThenReturnCorrectSongs()
        {
            // Arr
            BandDto bandDto = new BandDto() { Band_name = "a" };
            SongDto a = new SongDto() { Band = "a" };
            SongDto b = new SongDto() { Band = "a" };
            SongDto c = new SongDto() { Band = "c" };
            MockRepository.Setup(m => m.ListAllBands()).Returns(new List<BandDto> { bandDto });
            MockRepository.Setup(m => m.ListAllSongs()).Returns(new List<SongDto> { a, b, c });
            Logic = new LogicImpl(MockRepository.Object);

            // Act
            var result = Logic.ListAllSongsByBand("a");

            // Assert
            Assert.That(result.Count(), Is.EqualTo(2));
            Assert.That(result.Contains(a) && result.Contains(b));
        }

        /// <summary>
        /// WhenListAllSongsByBand AndInvalidParameter ThenThrowsException
        /// </summary>
        [Test]
        public void WhenListAllSongsByBand_AndInvalidParameter_ThenThrowsException()
        {
            // Arr
            SongDto a = new SongDto() { Title = "a", Album = "A" };
            SongDto b = new SongDto() { Title = "b", Album = "A" };
            SongDto c = new SongDto() { Title = "c", Album = "B" };
            MockRepository.Setup(m => m.ListAllSongs()).Returns(new List<SongDto> { a, b, c });
            Logic = new LogicImpl(MockRepository.Object);

            // Act
            var ex = Assert.Throws<BandException>(() => Logic.ListAllSongsByBand("c"));

            // Assert
            Assert.That(ex.Message, Is.EqualTo("There is no band with this name."));
        }

        /// <summary>
        /// WhenListAllSongsByBand AndNoSongInDb ThenThrowsException
        /// </summary>
        [Test]
        public void WhenListAllSongsByBand_AndNoSongInDb_ThenThrowsException()
        {
            // Arr
            BandDto bandDto = new BandDto() { Band_name = "a" };
            MockRepository.Setup(m => m.ListAllBands()).Returns(new List<BandDto> { bandDto });
            Logic = new LogicImpl(MockRepository.Object);

            // Act
            var ex = Assert.Throws<SongException>(() => Logic.ListAllSongsByBand("a"));

            // Assert
            Assert.That(ex.Message, Is.EqualTo("There is no song from this band."));
        }

        /// <summary>
        /// WhenListBandsByGenre ThenReturnCorrectBands
        /// </summary>
        [Test]
        public void WhenListBandsByGenre_ThenReturnCorrectBands()
        {
            // Arr
            BandDto a = new BandDto() { Band_name = "a" };
            BandDto b = new BandDto() { Band_name = "b" };
            BandDto c = new BandDto() { Band_name = "c" };
            SongDto songA = new SongDto() { Band = "a", Genre = "djent" };
            SongDto songB = new SongDto() { Band = "b", Genre = "djent" };
            SongDto songC = new SongDto() { Band = "c", Genre = "deathcore" };
            MockRepository.Setup(m => m.ListAllBands()).Returns(new List<BandDto> { a, b, c });
            MockRepository.Setup(m => m.ListAllSongs()).Returns(new List<SongDto> { songA, songB, songC });
            Logic = new LogicImpl(MockRepository.Object);

            // Act
            var result = Logic.ListBandsByGenre("djent");

            // Assert
            Assert.That(result.Count() == 2);
            Assert.That(result.Contains(a) && result.Contains(b));
        }

        /// <summary>
        /// WhenListBandsByGenre AndNoBandInGenre ThenThrowsException
        /// </summary>
        [Test]
        public void WhenListBandsByGenre_AndNoBandInGenre_ThenThrowsException()
        {
            // Arr
            Logic = new LogicImpl(MockRepository.Object);

            // Act
            var ex = Assert.Throws<GenreException>(() => Logic.ListBandsByGenre("djent"));

            // Assert
            Assert.That(ex.Message, Is.EqualTo("There is no band in this genre."));
        }

        /// <summary>
        /// WhenListMusiciansByBand ThenReturnCorrectMusicians
        /// </summary>
        [Test]
        public void WhenListMusiciansByBand_ThenReturnCorrectMusicians()
        {
            // Arr
            MusicianDto a = new MusicianDto() { Musician_name = "a" };
            MusicianDto b = new MusicianDto() { Musician_name = "b" };
            MusicianDto c = new MusicianDto() { Musician_name = "c" };
            MembershipDto mmbship1 = new MembershipDto() { Musician_name = "a", Band = "band" };
            MembershipDto mmbship2 = new MembershipDto() { Musician_name = "b", Band = "band" };
            BandDto band = new BandDto() { Band_name = "band" };
            MockRepository.Setup(m => m.ListAllBands()).Returns(new List<BandDto> { band });
            MockRepository.Setup(m => m.ListAllMusicians()).Returns(new List<MusicianDto> { a, b, c });
            MockRepository.Setup(m => m.ListAllMemberships()).Returns(new List<MembershipDto> { mmbship1, mmbship2 });
            Logic = new LogicImpl(MockRepository.Object);

            // Act
            var result = Logic.ListMusiciansByBand("band");

            // Assert
            Assert.That(result.Count() == 2);
            Assert.That(result.Contains(a) && result.Contains(b));
        }

        /// <summary>
        /// WhenListMusiciansByBand AndNoMusicians ThenThrowsException
        /// </summary>
        [Test]
        public void WhenListMusiciansByBand_AndNoMusicians_ThenThrowsException()
        {
            // Arr
            BandDto band = new BandDto() { Band_name = "band" };
            MockRepository.Setup(m => m.ListAllBands()).Returns(new List<BandDto> { band });
            Logic = new LogicImpl(MockRepository.Object);

            // Act
            var ex = Assert.Throws<MusicianException>(() => Logic.ListMusiciansByBand("band"));

            // Assert
            Assert.That(ex.Message, Is.EqualTo("There is no members in this band."));
        }

        /// <summary>
        /// WhenListMusiciansPerformedOnAlbum ThenReturnCorrectMusicians
        /// </summary>
        [Test]
        public void WhenListMusiciansPerformedOnAlbum_ThenReturnCorrectMusicians()
        {
            // Arr
            MusicianDto a = new MusicianDto() { Musician_name = "a" };
            MusicianDto b = new MusicianDto() { Musician_name = "b" };
            MusicianDto c = new MusicianDto() { Musician_name = "c" };
            AlbumDto album = new AlbumDto() { Album_name = "album", BandName = "band" };
            BandDto band = new BandDto() { Band_name = "band" };
            MembershipDto mmbship1 = new MembershipDto() { Musician_name = "a", Band = "band" };
            MembershipDto mmbship2 = new MembershipDto() { Musician_name = "b", Band = "band" };
            MembershipDto mmbship3 = new MembershipDto() { Musician_name = "c", Band = "choir" };
            MockRepository.Setup(m => m.ListAllMusicians()).Returns(new List<MusicianDto> { a, b, c });
            MockRepository.Setup(m => m.ListAllAlbums()).Returns(new List<AlbumDto> { album });
            MockRepository.Setup(m => m.ListAllBands()).Returns(new List<BandDto> { band });
            MockRepository.Setup(m => m.ListAllMemberships()).Returns(new List<MembershipDto> { mmbship1, mmbship2, mmbship3 });
            Logic = new LogicImpl(MockRepository.Object);

            // Act
            var result = Logic.ListMusiciansPerformedOnAlbum("album");

            // Assert
            Assert.That(result.Count() == 2);
            Assert.That(result.Contains(a) && result.Contains(b));
        }

        /// <summary>
        /// WhenListMusiciansByReleasingUnderLabel ThenReturnCorrectMusicians
        /// </summary>
        [Test]
        public void WhenListMusiciansByReleasingUnderLabel_ThenReturnCorrectMusicians()
        {
            // Arr
            LabelDto label = new LabelDto() { Label_name = "label" };
            AlbumDto album1 = new AlbumDto() { Album_name = "album1", Label = "label", BandName = "band1" };
            AlbumDto album2 = new AlbumDto() { Album_name = "album2", Label = "label", BandName = "band2" };
            BandDto band1 = new BandDto() { Band_name = "band1" };
            BandDto band2 = new BandDto() { Band_name = "band2" };
            MembershipDto mmbship1 = new MembershipDto() { Musician_name = "a", Band = "band1" };
            MembershipDto mmbship2 = new MembershipDto() { Musician_name = "b", Band = "band2" };
            MembershipDto mmbship3 = new MembershipDto() { Musician_name = "c", Band = "choir" };
            MusicianDto a = new MusicianDto() { Musician_name = "a" };
            MusicianDto b = new MusicianDto() { Musician_name = "b" };
            MusicianDto c = new MusicianDto() { Musician_name = "c" };
            MockRepository.Setup(m => m.ListAllLabels()).Returns(new List<LabelDto> { label });
            MockRepository.Setup(m => m.ListAllMusicians()).Returns(new List<MusicianDto> { a, b, c });
            MockRepository.Setup(m => m.ListAllAlbums()).Returns(new List<AlbumDto> { album1, album2 });
            MockRepository.Setup(m => m.ListAllBands()).Returns(new List<BandDto> { band1, band2 });
            MockRepository.Setup(m => m.ListAllMemberships()).Returns(new List<MembershipDto> { mmbship1, mmbship2, mmbship3 });
            Logic = new LogicImpl(MockRepository.Object);

            // Act
            var result = Logic.ListMusiciansByReleasingUnderLabel("label");

            // Assert
            Assert.That(result.Count() == 2);
            Assert.That(result.Contains(a) && result.Contains(b));
        }

        /// <summary>
        /// WhenListMembershipsByMusician ThenReturnCorrectMemberships
        /// </summary>
        [Test]
        public void WhenListMembershipsByMusician_ThenReturnCorrectMemberships()
        {
            // Arr
            MembershipDto ms1 = new MembershipDto() { Musician_name = "a", Band = "band1" };
            MembershipDto ms2 = new MembershipDto() { Musician_name = "a", Band = "band2" };
            MembershipDto ms3 = new MembershipDto() { Musician_name = "a", Band = "choir" };
            MockRepository.Setup(m => m.ListAllMemberships()).Returns(new List<MembershipDto> { ms1, ms2, ms3 });
            Logic = new LogicImpl(MockRepository.Object);

            // Act
            var result = Logic.ListMembershipsByMusician("a");

            // Assert
            Assert.That(result.Count() == 3);
            Assert.That(result.Contains(ms1) && result.Contains(ms2) && result.Contains(ms3));
        }

        /// <summary>
        /// WhenListMembershipsByBand ThenReturnCorrectMemberships
        /// </summary>
        [Test]
        public void WhenListMembershipsByBand_ThenReturnCorrectMemberships()
        {
            // Arr
            MembershipDto ms1 = new MembershipDto() { Musician_name = "a", Band = "band" };
            MembershipDto ms2 = new MembershipDto() { Musician_name = "b", Band = "band" };
            MembershipDto ms3 = new MembershipDto() { Musician_name = "c", Band = "band" };
            MockRepository.Setup(m => m.ListAllMemberships()).Returns(new List<MembershipDto> { ms1, ms2, ms3 });
            Logic = new LogicImpl(MockRepository.Object);

            // Act
            var result = Logic.ListMembershipsByBand("band");

            // Assert
            Assert.That(result.Count() == 3);
            Assert.That(result.Contains(ms1) && result.Contains(ms2) && result.Contains(ms3));
        }
    }
}
