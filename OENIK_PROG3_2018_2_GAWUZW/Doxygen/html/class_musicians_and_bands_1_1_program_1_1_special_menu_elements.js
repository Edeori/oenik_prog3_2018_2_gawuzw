var class_musicians_and_bands_1_1_program_1_1_special_menu_elements =
[
    [ "SpecialMenuElements", "class_musicians_and_bands_1_1_program_1_1_special_menu_elements.html#a5af5d66da0e2d5cb99d20d532805c687", null ],
    [ "BandsByGenre", "class_musicians_and_bands_1_1_program_1_1_special_menu_elements.html#ab242ffca76fe4cb34aa467fe16080c35", null ],
    [ "MusiciansByBand", "class_musicians_and_bands_1_1_program_1_1_special_menu_elements.html#a6442c443c4dc9e8ab24855afd684a83b", null ],
    [ "MusiciansOnAlbum", "class_musicians_and_bands_1_1_program_1_1_special_menu_elements.html#a0fec2e9651b3fa72dabb87954c687241", null ],
    [ "MusiciansUnderLabel", "class_musicians_and_bands_1_1_program_1_1_special_menu_elements.html#a9e209eda0bbd540e3ad9e355ebf54d50", null ],
    [ "SongsByAlbum", "class_musicians_and_bands_1_1_program_1_1_special_menu_elements.html#a18b447745d9ebc5224a8103abe78d47e", null ],
    [ "SongsByBand", "class_musicians_and_bands_1_1_program_1_1_special_menu_elements.html#ac6d30e949fcbce16850820f4998fc65b", null ],
    [ "Logic", "class_musicians_and_bands_1_1_program_1_1_special_menu_elements.html#a40c8471aafa914de12630494b7844aa9", null ]
];