var dir_528959697a4993ef33a8496830d9ea0c =
[
    [ "Dtos", "dir_024b860691aa93f6380639c7d423c01a.html", "dir_024b860691aa93f6380639c7d423c01a" ],
    [ "MusiciansAndBands.Data", "dir_5acc7321e21fd6c165c8d7e628a9b6fb.html", "dir_5acc7321e21fd6c165c8d7e628a9b6fb" ],
    [ "MusiciansAndBands.Logic", "dir_da52077ceb0b9c20f5a5c2fd75e27bbf.html", "dir_da52077ceb0b9c20f5a5c2fd75e27bbf" ],
    [ "MusiciansAndBands.Logic.Tests", "dir_bffae8926a2a362dbe47f4e304268fc5.html", "dir_bffae8926a2a362dbe47f4e304268fc5" ],
    [ "MusiciansAndBands.Program", "dir_5b433740c37102bc89a72529e5728fdc.html", "dir_5b433740c37102bc89a72529e5728fdc" ],
    [ "MusiciansAndBands.Repository", "dir_29a9a0d956ab2a9ce7a5c5197d5092b8.html", "dir_29a9a0d956ab2a9ce7a5c5197d5092b8" ],
    [ "MusiciansAndBands.Repository.Tests", "dir_a187f3ed1f7f20d4e61092b6ddb2c26e.html", "dir_a187f3ed1f7f20d4e61092b6ddb2c26e" ],
    [ "MusiciansAndBands.Shared", "dir_0852f98e53063742617104f10e4e8c2f.html", "dir_0852f98e53063742617104f10e4e8c2f" ],
    [ "MusiciansAndBandsApi", "dir_dbf79e04927f48df5918e7442ec130e5.html", "dir_dbf79e04927f48df5918e7442ec130e5" ],
    [ "MusiciansAndBandsApi.Tests", "dir_a185a1d0e6360bb5a28198559c3d5b76.html", "dir_a185a1d0e6360bb5a28198559c3d5b76" ],
    [ "MusiciansAndBandsService", "dir_334d06914bc9193daf543a019bb36e28.html", "dir_334d06914bc9193daf543a019bb36e28" ],
    [ "OENIK_PROG3_2018_2_GAWUZW", "dir_8fdb7a568fd6826b9f5208f4f4e8ae7a.html", "dir_8fdb7a568fd6826b9f5208f4f4e8ae7a" ]
];