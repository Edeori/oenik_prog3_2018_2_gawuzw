var namespace_musicians_and_bands_1_1_logic =
[
    [ "Tests", "namespace_musicians_and_bands_1_1_logic_1_1_tests.html", "namespace_musicians_and_bands_1_1_logic_1_1_tests" ],
    [ "Create", "class_musicians_and_bands_1_1_logic_1_1_create.html", "class_musicians_and_bands_1_1_logic_1_1_create" ],
    [ "ILogic", "interface_musicians_and_bands_1_1_logic_1_1_i_logic.html", "interface_musicians_and_bands_1_1_logic_1_1_i_logic" ],
    [ "InputReader", "class_musicians_and_bands_1_1_logic_1_1_input_reader.html", "class_musicians_and_bands_1_1_logic_1_1_input_reader" ],
    [ "LoadData", "class_musicians_and_bands_1_1_logic_1_1_load_data.html", "class_musicians_and_bands_1_1_logic_1_1_load_data" ],
    [ "LogicImpl", "class_musicians_and_bands_1_1_logic_1_1_logic_impl.html", "class_musicians_and_bands_1_1_logic_1_1_logic_impl" ]
];