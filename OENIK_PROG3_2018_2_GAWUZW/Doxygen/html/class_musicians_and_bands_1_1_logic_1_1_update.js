var class_musicians_and_bands_1_1_logic_1_1_update =
[
    [ "Update", "class_musicians_and_bands_1_1_logic_1_1_update.html#abbdfcfb0f9db1fa787eac579135eb2a6", null ],
    [ "UpdateAlbum", "class_musicians_and_bands_1_1_logic_1_1_update.html#a4a6921d97105ac6551619462bdabe6f9", null ],
    [ "UpdateBand", "class_musicians_and_bands_1_1_logic_1_1_update.html#a716699bc2130921d62f28ef2d1b369b1", null ],
    [ "UpdateLabel", "class_musicians_and_bands_1_1_logic_1_1_update.html#a6967f1cf0423e4a188d2d2d7e383fb59", null ],
    [ "UpdateMusician", "class_musicians_and_bands_1_1_logic_1_1_update.html#a2e4c743aa39f9c859782409289bad854", null ],
    [ "UpdateSong", "class_musicians_and_bands_1_1_logic_1_1_update.html#a22aa0415fd3a98d4a8684ade7311b53f", null ],
    [ "InputReader", "class_musicians_and_bands_1_1_logic_1_1_update.html#ac034cb0e10c83c668c21d8021ea8291c", null ],
    [ "Repository", "class_musicians_and_bands_1_1_logic_1_1_update.html#a5bf17572252ae93ee758f883cfdc5fde", null ]
];