var dir_96ef372fa2d35fd97ec6062a495544dc =
[
    [ "obj", "dir_b55fa1fabcec4c790ce8b791b1172207.html", "dir_b55fa1fabcec4c790ce8b791b1172207" ],
    [ "Properties", "dir_547cc7f65746a5a60d15ea694f3822e8.html", "dir_547cc7f65746a5a60d15ea694f3822e8" ],
    [ "AlbumException.cs", "_album_exception_8cs_source.html", null ],
    [ "BandException.cs", "_band_exception_8cs_source.html", null ],
    [ "GenreException.cs", "_genre_exception_8cs_source.html", null ],
    [ "LabelException.cs", "_label_exception_8cs_source.html", null ],
    [ "MembersipException.cs", "_membersip_exception_8cs_source.html", null ],
    [ "MusicianException.cs", "_musician_exception_8cs_source.html", null ],
    [ "SongException.cs", "_song_exception_8cs_source.html", null ],
    [ "StringsToConsole.cs", "_strings_to_console_8cs_source.html", null ]
];