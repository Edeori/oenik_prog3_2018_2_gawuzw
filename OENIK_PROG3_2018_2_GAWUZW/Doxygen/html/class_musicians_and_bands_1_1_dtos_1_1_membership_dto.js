var class_musicians_and_bands_1_1_dtos_1_1_membership_dto =
[
    [ "Band", "class_musicians_and_bands_1_1_dtos_1_1_membership_dto.html#a58abf28beaacd46511ee286ca45ffc01", null ],
    [ "Band_ID", "class_musicians_and_bands_1_1_dtos_1_1_membership_dto.html#ae68090811d21e695883cf8e41c0cfe12", null ],
    [ "Date_of_birth", "class_musicians_and_bands_1_1_dtos_1_1_membership_dto.html#a7ac171b3f6f72a4bc3ce7fdcf5d53d39", null ],
    [ "Membership_ID", "class_musicians_and_bands_1_1_dtos_1_1_membership_dto.html#ad431c5598890cd0e28edd7a1d44f4cd1", null ],
    [ "Musician_name", "class_musicians_and_bands_1_1_dtos_1_1_membership_dto.html#a0aebbccac81bbf3bd0765b93bc78948a", null ],
    [ "Position", "class_musicians_and_bands_1_1_dtos_1_1_membership_dto.html#a55f804c99c78e31fcd5807e2a864d694", null ]
];