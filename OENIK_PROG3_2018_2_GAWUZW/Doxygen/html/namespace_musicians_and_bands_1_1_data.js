var namespace_musicians_and_bands_1_1_data =
[
    [ "Albums", "class_musicians_and_bands_1_1_data_1_1_albums.html", "class_musicians_and_bands_1_1_data_1_1_albums" ],
    [ "Bands", "class_musicians_and_bands_1_1_data_1_1_bands.html", "class_musicians_and_bands_1_1_data_1_1_bands" ],
    [ "Labels", "class_musicians_and_bands_1_1_data_1_1_labels.html", "class_musicians_and_bands_1_1_data_1_1_labels" ],
    [ "Memberships", "class_musicians_and_bands_1_1_data_1_1_memberships.html", "class_musicians_and_bands_1_1_data_1_1_memberships" ],
    [ "Musicians", "class_musicians_and_bands_1_1_data_1_1_musicians.html", "class_musicians_and_bands_1_1_data_1_1_musicians" ],
    [ "MusiciansAndBandsDbEntities1", "class_musicians_and_bands_1_1_data_1_1_musicians_and_bands_db_entities1.html", "class_musicians_and_bands_1_1_data_1_1_musicians_and_bands_db_entities1" ],
    [ "Songs", "class_musicians_and_bands_1_1_data_1_1_songs.html", "class_musicians_and_bands_1_1_data_1_1_songs" ]
];