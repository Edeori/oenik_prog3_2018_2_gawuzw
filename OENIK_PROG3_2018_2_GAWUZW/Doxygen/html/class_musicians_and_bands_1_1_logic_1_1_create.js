var class_musicians_and_bands_1_1_logic_1_1_create =
[
    [ "Create", "class_musicians_and_bands_1_1_logic_1_1_create.html#a483ce372d55288883a56c1b6741db004", null ],
    [ "CreateAlbum", "class_musicians_and_bands_1_1_logic_1_1_create.html#ac9fe935a983b7b4ad581213bfab7558b", null ],
    [ "CreateBand", "class_musicians_and_bands_1_1_logic_1_1_create.html#aa65777cd14b7b93f21a52ef57ffdbebd", null ],
    [ "CreateLabel", "class_musicians_and_bands_1_1_logic_1_1_create.html#a4eaa1d3f144cb00201fa7ffa877cb8df", null ],
    [ "CreateMusician", "class_musicians_and_bands_1_1_logic_1_1_create.html#aca8cc24b10332dd790f029c811af414d", null ],
    [ "CreateSong", "class_musicians_and_bands_1_1_logic_1_1_create.html#a81ba85682ada88dcdbbc751965bfccd6", null ],
    [ "MultipleAlbumsCreate", "class_musicians_and_bands_1_1_logic_1_1_create.html#a5cac9291f0386f058b5663a12a7b8b84", null ],
    [ "MultipleBandsCreate", "class_musicians_and_bands_1_1_logic_1_1_create.html#a20ec3511569d116dc04abb8b53e509d0", null ],
    [ "MultipleLabelsCreate", "class_musicians_and_bands_1_1_logic_1_1_create.html#adc797e971a5783c10690663446735cb5", null ],
    [ "MultipleMusiciansCreate", "class_musicians_and_bands_1_1_logic_1_1_create.html#a120fd8cedef05527bc88c1e87e373327", null ],
    [ "MultipleSongsCreate", "class_musicians_and_bands_1_1_logic_1_1_create.html#ac6bf4616e5e9b7cd84eb811e18bd4096", null ],
    [ "InputReader", "class_musicians_and_bands_1_1_logic_1_1_create.html#a670b5e07894e235517674f2316accbce", null ],
    [ "Logic", "class_musicians_and_bands_1_1_logic_1_1_create.html#a67d9f8ba97b033512eb168915d7e6a5e", null ]
];