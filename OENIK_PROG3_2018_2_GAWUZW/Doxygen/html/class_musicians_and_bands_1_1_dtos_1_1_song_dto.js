var class_musicians_and_bands_1_1_dtos_1_1_song_dto =
[
    [ "ToString", "class_musicians_and_bands_1_1_dtos_1_1_song_dto.html#a62958bd1e573d20a801d4cc17788052b", null ],
    [ "ToString", "class_musicians_and_bands_1_1_dtos_1_1_song_dto.html#a62958bd1e573d20a801d4cc17788052b", null ],
    [ "Album", "class_musicians_and_bands_1_1_dtos_1_1_song_dto.html#aae8b69a2fcf3de51d619a5ddb1825e2d", null ],
    [ "Album_ID", "class_musicians_and_bands_1_1_dtos_1_1_song_dto.html#a50e0b1b9d8bfcbca0bd4b9e8eaf1b0d7", null ],
    [ "Band", "class_musicians_and_bands_1_1_dtos_1_1_song_dto.html#a22648af1ce48888c40ecd50393005d14", null ],
    [ "Band_ID", "class_musicians_and_bands_1_1_dtos_1_1_song_dto.html#af6bcc456e3894fcafcba417c3aa178a4", null ],
    [ "Genre", "class_musicians_and_bands_1_1_dtos_1_1_song_dto.html#a1c569ecbf44e87d74af1f57851290534", null ],
    [ "Song_ID", "class_musicians_and_bands_1_1_dtos_1_1_song_dto.html#a811b933b3b92e686967841f52f941cbc", null ],
    [ "Title", "class_musicians_and_bands_1_1_dtos_1_1_song_dto.html#af362f6dbcb6709ec26000e971e4cba5c", null ]
];