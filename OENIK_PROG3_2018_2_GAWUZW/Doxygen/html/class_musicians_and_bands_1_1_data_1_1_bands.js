var class_musicians_and_bands_1_1_data_1_1_bands =
[
    [ "Bands", "class_musicians_and_bands_1_1_data_1_1_bands.html#a887aaa01f978d274a0a954663c6c54e8", null ],
    [ "Albums", "class_musicians_and_bands_1_1_data_1_1_bands.html#ad42808c09d7323ad5d9545baeeddea7e", null ],
    [ "Band_ID", "class_musicians_and_bands_1_1_data_1_1_bands.html#ab52be16b5b00f6c1efe21b71d8f8c3b1", null ],
    [ "Band_name", "class_musicians_and_bands_1_1_data_1_1_bands.html#a9fe861ca29a2d5f32f2fe55edd26f99e", null ],
    [ "Form_date", "class_musicians_and_bands_1_1_data_1_1_bands.html#abcc4f0b08f2dda05aa0ddc22639962ea", null ],
    [ "Hometown", "class_musicians_and_bands_1_1_data_1_1_bands.html#adf6807fe54c7dfc285fea6a9fd6f4cb9", null ],
    [ "Label", "class_musicians_and_bands_1_1_data_1_1_bands.html#aa9abf50485dec02c6f94d7fe6a115391", null ],
    [ "Labels", "class_musicians_and_bands_1_1_data_1_1_bands.html#a587a901f62228d5977639c23f88e6c35", null ],
    [ "Memberships", "class_musicians_and_bands_1_1_data_1_1_bands.html#af7f4c3c09291acdd3291336971913afc", null ],
    [ "Songs", "class_musicians_and_bands_1_1_data_1_1_bands.html#ac70f5d17d1a35973511cce2df0a9f307", null ]
];