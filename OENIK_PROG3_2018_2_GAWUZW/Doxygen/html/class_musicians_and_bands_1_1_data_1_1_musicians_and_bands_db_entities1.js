var class_musicians_and_bands_1_1_data_1_1_musicians_and_bands_db_entities1 =
[
    [ "MusiciansAndBandsDbEntities1", "class_musicians_and_bands_1_1_data_1_1_musicians_and_bands_db_entities1.html#af1b775b60d7beeb2a48d510429ddd051", null ],
    [ "OnModelCreating", "class_musicians_and_bands_1_1_data_1_1_musicians_and_bands_db_entities1.html#a939b0cce6b3d72f22649c159b8bc345e", null ],
    [ "Albums", "class_musicians_and_bands_1_1_data_1_1_musicians_and_bands_db_entities1.html#a96f722cc519ac9bf54efd3b7a762090f", null ],
    [ "Bands", "class_musicians_and_bands_1_1_data_1_1_musicians_and_bands_db_entities1.html#a6eec4f848533d2ad3b0e105b8cb6a44f", null ],
    [ "Labels", "class_musicians_and_bands_1_1_data_1_1_musicians_and_bands_db_entities1.html#a211fefb417df8fa53aebf763a8efe253", null ],
    [ "Memberships", "class_musicians_and_bands_1_1_data_1_1_musicians_and_bands_db_entities1.html#ab048000b50cdbd8451c17b2d80687125", null ],
    [ "Musicians", "class_musicians_and_bands_1_1_data_1_1_musicians_and_bands_db_entities1.html#a1763daf1a3606ab5345726ef4c74e101", null ],
    [ "Songs", "class_musicians_and_bands_1_1_data_1_1_musicians_and_bands_db_entities1.html#a47829515efb28382aec99052388c428a", null ]
];