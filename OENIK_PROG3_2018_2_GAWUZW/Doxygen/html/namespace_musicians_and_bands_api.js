var namespace_musicians_and_bands_api =
[
    [ "Areas", "namespace_musicians_and_bands_api_1_1_areas.html", "namespace_musicians_and_bands_api_1_1_areas" ],
    [ "Controllers", "namespace_musicians_and_bands_api_1_1_controllers.html", "namespace_musicians_and_bands_api_1_1_controllers" ],
    [ "Tests", "namespace_musicians_and_bands_api_1_1_tests.html", "namespace_musicians_and_bands_api_1_1_tests" ],
    [ "BundleConfig", "class_musicians_and_bands_api_1_1_bundle_config.html", "class_musicians_and_bands_api_1_1_bundle_config" ],
    [ "FilterConfig", "class_musicians_and_bands_api_1_1_filter_config.html", "class_musicians_and_bands_api_1_1_filter_config" ],
    [ "IMusiciansAndBands", "interface_musicians_and_bands_api_1_1_i_musicians_and_bands.html", "interface_musicians_and_bands_api_1_1_i_musicians_and_bands" ],
    [ "MusiciansAndBands", "class_musicians_and_bands_api_1_1_musicians_and_bands.html", "class_musicians_and_bands_api_1_1_musicians_and_bands" ],
    [ "MusiciansAndBandsWebService", "class_musicians_and_bands_api_1_1_musicians_and_bands_web_service.html", "class_musicians_and_bands_api_1_1_musicians_and_bands_web_service" ],
    [ "RouteConfig", "class_musicians_and_bands_api_1_1_route_config.html", "class_musicians_and_bands_api_1_1_route_config" ],
    [ "WebApiApplication", "class_musicians_and_bands_api_1_1_web_api_application.html", "class_musicians_and_bands_api_1_1_web_api_application" ],
    [ "WebApiConfig", "class_musicians_and_bands_api_1_1_web_api_config.html", "class_musicians_and_bands_api_1_1_web_api_config" ]
];