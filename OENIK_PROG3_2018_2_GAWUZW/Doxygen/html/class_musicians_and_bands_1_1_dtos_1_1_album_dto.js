var class_musicians_and_bands_1_1_dtos_1_1_album_dto =
[
    [ "ToString", "class_musicians_and_bands_1_1_dtos_1_1_album_dto.html#ade0f7567b641f2e82d19d03a010fa272", null ],
    [ "ToString", "class_musicians_and_bands_1_1_dtos_1_1_album_dto.html#ade0f7567b641f2e82d19d03a010fa272", null ],
    [ "Album_ID", "class_musicians_and_bands_1_1_dtos_1_1_album_dto.html#af55edd59b2c8ac04c953d991eafe0c8f", null ],
    [ "Album_name", "class_musicians_and_bands_1_1_dtos_1_1_album_dto.html#a336ae87564a03a47d7c23ab5d0cb9c0f", null ],
    [ "BandId", "class_musicians_and_bands_1_1_dtos_1_1_album_dto.html#aef9f61cd5bac246279a22b01df9288c5", null ],
    [ "BandName", "class_musicians_and_bands_1_1_dtos_1_1_album_dto.html#a5c1716bd2a8c5c54cc7b81c232ca9d00", null ],
    [ "Label", "class_musicians_and_bands_1_1_dtos_1_1_album_dto.html#aa26ee177a519c825cd02b36e29042d88", null ],
    [ "Producer", "class_musicians_and_bands_1_1_dtos_1_1_album_dto.html#a4fb0e4a56cdee7ef69696dadcce44209", null ],
    [ "Year_Released", "class_musicians_and_bands_1_1_dtos_1_1_album_dto.html#aaa7e1131ea883ba0fddfe1ee2b768615", null ]
];