var namespace_musicians_and_bands_1_1_dtos =
[
    [ "AlbumDto", "class_musicians_and_bands_1_1_dtos_1_1_album_dto.html", "class_musicians_and_bands_1_1_dtos_1_1_album_dto" ],
    [ "BandDto", "class_musicians_and_bands_1_1_dtos_1_1_band_dto.html", "class_musicians_and_bands_1_1_dtos_1_1_band_dto" ],
    [ "BaseDto", "class_musicians_and_bands_1_1_dtos_1_1_base_dto.html", null ],
    [ "LabelDto", "class_musicians_and_bands_1_1_dtos_1_1_label_dto.html", "class_musicians_and_bands_1_1_dtos_1_1_label_dto" ],
    [ "MembershipDto", "class_musicians_and_bands_1_1_dtos_1_1_membership_dto.html", "class_musicians_and_bands_1_1_dtos_1_1_membership_dto" ],
    [ "MusicianDto", "class_musicians_and_bands_1_1_dtos_1_1_musician_dto.html", "class_musicians_and_bands_1_1_dtos_1_1_musician_dto" ],
    [ "SongDto", "class_musicians_and_bands_1_1_dtos_1_1_song_dto.html", "class_musicians_and_bands_1_1_dtos_1_1_song_dto" ]
];