var namespace_musicians_and_bands =
[
    [ "Data", "namespace_musicians_and_bands_1_1_data.html", "namespace_musicians_and_bands_1_1_data" ],
    [ "Dtos", "namespace_musicians_and_bands_1_1_dtos.html", "namespace_musicians_and_bands_1_1_dtos" ],
    [ "Logic", "namespace_musicians_and_bands_1_1_logic.html", "namespace_musicians_and_bands_1_1_logic" ],
    [ "Program", "namespace_musicians_and_bands_1_1_program.html", "namespace_musicians_and_bands_1_1_program" ],
    [ "Repository", "namespace_musicians_and_bands_1_1_repository.html", "namespace_musicians_and_bands_1_1_repository" ],
    [ "Shared", "namespace_musicians_and_bands_1_1_shared.html", "namespace_musicians_and_bands_1_1_shared" ]
];