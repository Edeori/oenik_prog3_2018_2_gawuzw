var dir_5f5e5f01e524f129e89dd10bfaec0d6f =
[
    [ "obj", "dir_c5429f7e07f2058612fb8eda43a6d9a3.html", "dir_c5429f7e07f2058612fb8eda43a6d9a3" ],
    [ "Properties", "dir_fd351a59d27e26059337766de47455ed.html", "dir_fd351a59d27e26059337766de47455ed" ],
    [ "AlbumDto.cs", "_musicians_and_bands_8_dtos_2_album_dto_8cs_source.html", null ],
    [ "BandDto.cs", "_musicians_and_bands_8_dtos_2_band_dto_8cs_source.html", null ],
    [ "BaseDto.cs", "_musicians_and_bands_8_dtos_2_base_dto_8cs_source.html", null ],
    [ "GlobalSuppressions.cs", "_musicians_and_bands_8_dtos_2_global_suppressions_8cs_source.html", null ],
    [ "LabelDto.cs", "_musicians_and_bands_8_dtos_2_label_dto_8cs_source.html", null ],
    [ "MembershipDto.cs", "_musicians_and_bands_8_dtos_2_membership_dto_8cs_source.html", null ],
    [ "MusicianDto.cs", "_musicians_and_bands_8_dtos_2_musician_dto_8cs_source.html", null ],
    [ "SongDto.cs", "_musicians_and_bands_8_dtos_2_song_dto_8cs_source.html", null ]
];