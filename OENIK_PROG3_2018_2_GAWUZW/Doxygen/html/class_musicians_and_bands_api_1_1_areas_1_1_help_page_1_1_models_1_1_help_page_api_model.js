var class_musicians_and_bands_api_1_1_areas_1_1_help_page_1_1_models_1_1_help_page_api_model =
[
    [ "HelpPageApiModel", "class_musicians_and_bands_api_1_1_areas_1_1_help_page_1_1_models_1_1_help_page_api_model.html#a35aeac34b4f97628f11cf33957853033", null ],
    [ "ApiDescription", "class_musicians_and_bands_api_1_1_areas_1_1_help_page_1_1_models_1_1_help_page_api_model.html#adfdc3f202e315f9f1c412f9cf6d78443", null ],
    [ "ErrorMessages", "class_musicians_and_bands_api_1_1_areas_1_1_help_page_1_1_models_1_1_help_page_api_model.html#af63feb9e2f1dc0061ae2d20892e352eb", null ],
    [ "RequestBodyParameters", "class_musicians_and_bands_api_1_1_areas_1_1_help_page_1_1_models_1_1_help_page_api_model.html#a5a8830ae9642a6d3928c45721ae3ff01", null ],
    [ "RequestDocumentation", "class_musicians_and_bands_api_1_1_areas_1_1_help_page_1_1_models_1_1_help_page_api_model.html#a6752b2d4c2fc636c6039a65a823dc015", null ],
    [ "RequestModelDescription", "class_musicians_and_bands_api_1_1_areas_1_1_help_page_1_1_models_1_1_help_page_api_model.html#a5bbef86d564d17be86cd4e5472240975", null ],
    [ "ResourceDescription", "class_musicians_and_bands_api_1_1_areas_1_1_help_page_1_1_models_1_1_help_page_api_model.html#aeba799efb74d88e7852cac2379636885", null ],
    [ "ResourceProperties", "class_musicians_and_bands_api_1_1_areas_1_1_help_page_1_1_models_1_1_help_page_api_model.html#ab27d8fde4e503d8c71079cff1d733d9d", null ],
    [ "SampleRequests", "class_musicians_and_bands_api_1_1_areas_1_1_help_page_1_1_models_1_1_help_page_api_model.html#a5e0868874b761b6c8234652dfe6ceb52", null ],
    [ "SampleResponses", "class_musicians_and_bands_api_1_1_areas_1_1_help_page_1_1_models_1_1_help_page_api_model.html#a13f968b51fe1268d2cee7ff22d03b55b", null ],
    [ "UriParameters", "class_musicians_and_bands_api_1_1_areas_1_1_help_page_1_1_models_1_1_help_page_api_model.html#a5b028acaffbe747f1c30fed1e81ef758", null ]
];