var class_musicians_and_bands_1_1_data_1_1_albums =
[
    [ "Albums", "class_musicians_and_bands_1_1_data_1_1_albums.html#a2d11e29a477860d7ff0e0b41c3e7c531", null ],
    [ "Album_ID", "class_musicians_and_bands_1_1_data_1_1_albums.html#a685200dce887315ed8f1096da041d454", null ],
    [ "Album_name", "class_musicians_and_bands_1_1_data_1_1_albums.html#ab99f9a50222739abcaf233c8b6937ffd", null ],
    [ "Band_ID", "class_musicians_and_bands_1_1_data_1_1_albums.html#a8a605f7f93026bcb642d0b2a151f7b41", null ],
    [ "Bands", "class_musicians_and_bands_1_1_data_1_1_albums.html#a6f324830262606e86441ab4a28637144", null ],
    [ "Label", "class_musicians_and_bands_1_1_data_1_1_albums.html#acaeedc988ba0c992bce68e3339ce9935", null ],
    [ "Labels", "class_musicians_and_bands_1_1_data_1_1_albums.html#abd61acea842fb7a362e26b47e44da53b", null ],
    [ "Producer", "class_musicians_and_bands_1_1_data_1_1_albums.html#ae0ada2aaa469a63f1522cfd621df5944", null ],
    [ "Songs", "class_musicians_and_bands_1_1_data_1_1_albums.html#abf6b4d5167a1fc0bdde80f3466d62fb2", null ],
    [ "Year_Released", "class_musicians_and_bands_1_1_data_1_1_albums.html#a89cc0e85dd0213ccc3d3ce3ed386f80a", null ]
];