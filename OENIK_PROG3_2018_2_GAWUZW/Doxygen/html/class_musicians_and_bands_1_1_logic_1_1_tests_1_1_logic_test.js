var class_musicians_and_bands_1_1_logic_1_1_tests_1_1_logic_test =
[
    [ "SetUp", "class_musicians_and_bands_1_1_logic_1_1_tests_1_1_logic_test.html#aa801efa817c5401100c3472c63d46e3e", null ],
    [ "WhenListAllAlbumsByBand_AndInvalidParameter_ThenThrowsException", "class_musicians_and_bands_1_1_logic_1_1_tests_1_1_logic_test.html#a6bbf4de7328507c715e6e292136beaaa", null ],
    [ "WhenListAllAlbumsByBand_AndNoAlbumInDb_ThenThrowsException", "class_musicians_and_bands_1_1_logic_1_1_tests_1_1_logic_test.html#a6e6bdb4ab05e80179c4c5445750867ee", null ],
    [ "WhenListAllAlbumsByBand_ThenReturnCorrectAlbums", "class_musicians_and_bands_1_1_logic_1_1_tests_1_1_logic_test.html#a94584f58d6b352d9ec596d94372e6561", null ],
    [ "WhenListAllSongsByAlbum_AndInvalidParameter_ThenThrowsException", "class_musicians_and_bands_1_1_logic_1_1_tests_1_1_logic_test.html#a1947f3dd33e0ee9f1d8733f9340995c4", null ],
    [ "WhenListAllSongsByAlbum_AndNoSongInDb_ThenThrowsException", "class_musicians_and_bands_1_1_logic_1_1_tests_1_1_logic_test.html#a96ad233102f32095fdc4650ccd24fead", null ],
    [ "WhenListAllSongsByAlbum_ThenReturnCorrectSongs", "class_musicians_and_bands_1_1_logic_1_1_tests_1_1_logic_test.html#a6f5d15393f96bbaf47655ad7fa128496", null ],
    [ "WhenListAllSongsByBand_AndInvalidParameter_ThenThrowsException", "class_musicians_and_bands_1_1_logic_1_1_tests_1_1_logic_test.html#af8720c4d15f5eea439438a02c9219473", null ],
    [ "WhenListAllSongsByBand_AndNoSongInDb_ThenThrowsException", "class_musicians_and_bands_1_1_logic_1_1_tests_1_1_logic_test.html#a60fe96df96b5a6ace36b68f95ae07517", null ],
    [ "WhenListAllSongsByBand_ThenReturnCorrectSongs", "class_musicians_and_bands_1_1_logic_1_1_tests_1_1_logic_test.html#aca8ae131b821d9d472bfda65c815b56c", null ],
    [ "WhenListBandsByGenre_AndNoBandInGenre_ThenThrowsException", "class_musicians_and_bands_1_1_logic_1_1_tests_1_1_logic_test.html#a95b157e9f4f331e35aadcd67a9a9b28d", null ],
    [ "WhenListBandsByGenre_ThenReturnCorrectBands", "class_musicians_and_bands_1_1_logic_1_1_tests_1_1_logic_test.html#a8ea70fe6744f999b6d9399b23ed21d85", null ],
    [ "WhenListMembershipsByBand_ThenReturnCorrectMemberships", "class_musicians_and_bands_1_1_logic_1_1_tests_1_1_logic_test.html#a1b07a5fbf82c118c8335b81137319151", null ],
    [ "WhenListMembershipsByMusician_ThenReturnCorrectMemberships", "class_musicians_and_bands_1_1_logic_1_1_tests_1_1_logic_test.html#aad1f6a54fd3633576a76f19ba06cac94", null ],
    [ "WhenListMusiciansByBand_AndNoMusicians_ThenThrowsException", "class_musicians_and_bands_1_1_logic_1_1_tests_1_1_logic_test.html#a58c3df8876a7e3a7f055ecf5b6174dbe", null ],
    [ "WhenListMusiciansByBand_ThenReturnCorrectMusicians", "class_musicians_and_bands_1_1_logic_1_1_tests_1_1_logic_test.html#aa64c3084a595233c57e42f35bda6c398", null ],
    [ "WhenListMusiciansByReleasingUnderLabel_ThenReturnCorrectMusicians", "class_musicians_and_bands_1_1_logic_1_1_tests_1_1_logic_test.html#a35071734f6933ad798a65f961fb75966", null ],
    [ "WhenListMusiciansPerformedOnAlbum_ThenReturnCorrectMusicians", "class_musicians_and_bands_1_1_logic_1_1_tests_1_1_logic_test.html#abbc9a949fa6b81c30c61e2e5b6fa8163", null ],
    [ "Logic", "class_musicians_and_bands_1_1_logic_1_1_tests_1_1_logic_test.html#aef25b1111b3da32387c6816fe6e5e81a", null ],
    [ "MockRepository", "class_musicians_and_bands_1_1_logic_1_1_tests_1_1_logic_test.html#a7fcb7a87d52cbcae4c99b0286241e7e0", null ]
];