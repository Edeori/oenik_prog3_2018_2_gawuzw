var hierarchy =
[
    [ "MusiciansAndBands.Data.Albums", "class_musicians_and_bands_1_1_data_1_1_albums.html", null ],
    [ "MusiciansAndBands.Data.Bands", "class_musicians_and_bands_1_1_data_1_1_bands.html", null ],
    [ "MusiciansAndBands.Dtos.BaseDto", "class_musicians_and_bands_1_1_dtos_1_1_base_dto.html", [
      [ "MusiciansAndBands.Dtos.AlbumDto", "class_musicians_and_bands_1_1_dtos_1_1_album_dto.html", null ],
      [ "MusiciansAndBands.Dtos.AlbumDto", "class_musicians_and_bands_1_1_dtos_1_1_album_dto.html", null ],
      [ "MusiciansAndBands.Dtos.BandDto", "class_musicians_and_bands_1_1_dtos_1_1_band_dto.html", null ],
      [ "MusiciansAndBands.Dtos.BandDto", "class_musicians_and_bands_1_1_dtos_1_1_band_dto.html", null ],
      [ "MusiciansAndBands.Dtos.LabelDto", "class_musicians_and_bands_1_1_dtos_1_1_label_dto.html", null ],
      [ "MusiciansAndBands.Dtos.LabelDto", "class_musicians_and_bands_1_1_dtos_1_1_label_dto.html", null ],
      [ "MusiciansAndBands.Dtos.MembershipDto", "class_musicians_and_bands_1_1_dtos_1_1_membership_dto.html", null ],
      [ "MusiciansAndBands.Dtos.MembershipDto", "class_musicians_and_bands_1_1_dtos_1_1_membership_dto.html", null ],
      [ "MusiciansAndBands.Dtos.MusicianDto", "class_musicians_and_bands_1_1_dtos_1_1_musician_dto.html", null ],
      [ "MusiciansAndBands.Dtos.MusicianDto", "class_musicians_and_bands_1_1_dtos_1_1_musician_dto.html", null ],
      [ "MusiciansAndBands.Dtos.SongDto", "class_musicians_and_bands_1_1_dtos_1_1_song_dto.html", null ],
      [ "MusiciansAndBands.Dtos.SongDto", "class_musicians_and_bands_1_1_dtos_1_1_song_dto.html", null ]
    ] ],
    [ "OENIK_PROG3_2018_2_GAWUZW.Class1", "class_o_e_n_i_k___p_r_o_g3__2018__2___g_a_w_u_z_w_1_1_class1.html", null ],
    [ "MusiciansAndBands.Logic.Create", "class_musicians_and_bands_1_1_logic_1_1_create.html", null ],
    [ "MusiciansAndBands.Program.CustomListExtensions", "class_musicians_and_bands_1_1_program_1_1_custom_list_extensions.html", null ],
    [ "DbContext", null, [
      [ "MusiciansAndBands.Data.MusiciansAndBandsDbEntities1", "class_musicians_and_bands_1_1_data_1_1_musicians_and_bands_db_entities1.html", null ]
    ] ],
    [ "MusiciansAndBands.Repository.DtoConverter", "class_musicians_and_bands_1_1_repository_1_1_dto_converter.html", null ],
    [ "Exception", null, [
      [ "MusiciansAndBands.Shared.AlbumException", "class_musicians_and_bands_1_1_shared_1_1_album_exception.html", null ],
      [ "MusiciansAndBands.Shared.BandException", "class_musicians_and_bands_1_1_shared_1_1_band_exception.html", null ],
      [ "MusiciansAndBands.Shared.GenreException", "class_musicians_and_bands_1_1_shared_1_1_genre_exception.html", null ],
      [ "MusiciansAndBands.Shared.LabelException", "class_musicians_and_bands_1_1_shared_1_1_label_exception.html", null ],
      [ "MusiciansAndBands.Shared.MembersipException", "class_musicians_and_bands_1_1_shared_1_1_membersip_exception.html", null ],
      [ "MusiciansAndBands.Shared.MusicianException", "class_musicians_and_bands_1_1_shared_1_1_musician_exception.html", null ],
      [ "MusiciansAndBands.Shared.SongException", "class_musicians_and_bands_1_1_shared_1_1_song_exception.html", null ]
    ] ],
    [ "MusiciansAndBands.Logic.ILogic", "interface_musicians_and_bands_1_1_logic_1_1_i_logic.html", [
      [ "MusiciansAndBands.Logic.LogicImpl", "class_musicians_and_bands_1_1_logic_1_1_logic_impl.html", null ]
    ] ],
    [ "MusiciansAndBands.Logic.InputReader", "class_musicians_and_bands_1_1_logic_1_1_input_reader.html", null ],
    [ "MusiciansAndBands.Repository.IRepository", "interface_musicians_and_bands_1_1_repository_1_1_i_repository.html", [
      [ "MusiciansAndBands.Repository.RepositoryImpl", "class_musicians_and_bands_1_1_repository_1_1_repository_impl.html", null ]
    ] ],
    [ "MusiciansAndBands.Data.Labels", "class_musicians_and_bands_1_1_data_1_1_labels.html", null ],
    [ "MusiciansAndBands.Logic.LoadData", "class_musicians_and_bands_1_1_logic_1_1_load_data.html", null ],
    [ "MusiciansAndBands.Logic.Tests.LogicTest", "class_musicians_and_bands_1_1_logic_1_1_tests_1_1_logic_test.html", null ],
    [ "MusiciansAndBands.Data.Memberships", "class_musicians_and_bands_1_1_data_1_1_memberships.html", null ],
    [ "MusiciansAndBands.Data.Musicians", "class_musicians_and_bands_1_1_data_1_1_musicians.html", null ],
    [ "MusiciansAndBands.Program.Program", "class_musicians_and_bands_1_1_program_1_1_program.html", null ],
    [ "MusiciansAndBands.Data.Songs", "class_musicians_and_bands_1_1_data_1_1_songs.html", null ],
    [ "MusiciansAndBands.Program.SpecialMenuElements", "class_musicians_and_bands_1_1_program_1_1_special_menu_elements.html", null ]
];