var interface_musicians_and_bands_1_1_logic_1_1_i_logic =
[
    [ "AddNewAlbum", "interface_musicians_and_bands_1_1_logic_1_1_i_logic.html#ae3a7a52c7d749a8fe282524c726c1d38", null ],
    [ "AddNewBand", "interface_musicians_and_bands_1_1_logic_1_1_i_logic.html#afa6157a4089087a5210193f491a7f29b", null ],
    [ "AddNewLabel", "interface_musicians_and_bands_1_1_logic_1_1_i_logic.html#aca8c6ad80e79c52456b344c16abe08c8", null ],
    [ "AddNewMusician", "interface_musicians_and_bands_1_1_logic_1_1_i_logic.html#a362ff47a8ccbccd8403a1d40f54075e7", null ],
    [ "AddNewSong", "interface_musicians_and_bands_1_1_logic_1_1_i_logic.html#abc8fd36019f60c98a811e0b0389f7505", null ],
    [ "DeleteAlbumById", "interface_musicians_and_bands_1_1_logic_1_1_i_logic.html#ad342d2d2f96a60521743351626f1d173", null ],
    [ "DeleteBandById", "interface_musicians_and_bands_1_1_logic_1_1_i_logic.html#af3b34d7d142a035bc7d48669dea621d8", null ],
    [ "DeleteLabelById", "interface_musicians_and_bands_1_1_logic_1_1_i_logic.html#aea3b1e36023a15c64f2045a798684350", null ],
    [ "DeleteMusicianById", "interface_musicians_and_bands_1_1_logic_1_1_i_logic.html#ae1923cbcd7425f1ee68b33ba4ee4caf4", null ],
    [ "DeleteSongById", "interface_musicians_and_bands_1_1_logic_1_1_i_logic.html#a22721d75e5dca26c5598f92fd8a844d2", null ],
    [ "GetAlbumById", "interface_musicians_and_bands_1_1_logic_1_1_i_logic.html#a46c81b373257e3b50d8fccf242d200b4", null ],
    [ "GetBandById", "interface_musicians_and_bands_1_1_logic_1_1_i_logic.html#ac010ff8c934c460290ffc75b7da498fa", null ],
    [ "GetLabelById", "interface_musicians_and_bands_1_1_logic_1_1_i_logic.html#a5aae29ed3d5c2aa7887fd962892b5d0f", null ],
    [ "GetMusicianById", "interface_musicians_and_bands_1_1_logic_1_1_i_logic.html#a747a0b9a13ca98f9e846a1b82d2de7bf", null ],
    [ "GetSongById", "interface_musicians_and_bands_1_1_logic_1_1_i_logic.html#a999324c9f75c32513a4b19ba49f17c72", null ],
    [ "ListAllAlbums", "interface_musicians_and_bands_1_1_logic_1_1_i_logic.html#a23ee47f19fbe43c797d50b2f005c1a77", null ],
    [ "ListAllAlbumsByBand", "interface_musicians_and_bands_1_1_logic_1_1_i_logic.html#ad3f5200a89376aa882ad33aa46ee2f01", null ],
    [ "ListAllBands", "interface_musicians_and_bands_1_1_logic_1_1_i_logic.html#a7a79647aac67c56d858e9f8771d380eb", null ],
    [ "ListAllLabels", "interface_musicians_and_bands_1_1_logic_1_1_i_logic.html#a411a4f511f639dc500fbc6bf7a654d4c", null ],
    [ "ListAllMusicians", "interface_musicians_and_bands_1_1_logic_1_1_i_logic.html#af76858a364ef7132b148c77d1f6806e7", null ],
    [ "ListAllSongs", "interface_musicians_and_bands_1_1_logic_1_1_i_logic.html#a2d9d1ce042ebadea65dfc8c2716d7cd5", null ],
    [ "ListAllSongsByAlbum", "interface_musicians_and_bands_1_1_logic_1_1_i_logic.html#a8369ad1fc904bbe864d694a4c57264e3", null ],
    [ "ListAllSongsByBand", "interface_musicians_and_bands_1_1_logic_1_1_i_logic.html#aa06faf43bf4900036bdb79ce347d3eb3", null ],
    [ "ListBandsByGenre", "interface_musicians_and_bands_1_1_logic_1_1_i_logic.html#adfca22e77436a1ce6b66316d6d44cbca", null ],
    [ "ListMembershipsByBand", "interface_musicians_and_bands_1_1_logic_1_1_i_logic.html#a4c8864e9d8123683f494095ffd42a987", null ],
    [ "ListMembershipsByMusician", "interface_musicians_and_bands_1_1_logic_1_1_i_logic.html#a5c4644109eac123d29d2b67ac2f56d17", null ],
    [ "ListMusiciansByBand", "interface_musicians_and_bands_1_1_logic_1_1_i_logic.html#a0e8aab8afd65546da675e4d643f60a60", null ],
    [ "ListMusiciansByReleasingUnderLabel", "interface_musicians_and_bands_1_1_logic_1_1_i_logic.html#a97d437233a5326a60df11ab7b1e307d4", null ],
    [ "ListMusiciansPerformedOnAlbum", "interface_musicians_and_bands_1_1_logic_1_1_i_logic.html#a7a47619ce51f522046e6e2b333c641b8", null ],
    [ "UpdateAlbumById", "interface_musicians_and_bands_1_1_logic_1_1_i_logic.html#a12e6b154313224abc92255d2b8ce2083", null ],
    [ "UpdateBandById", "interface_musicians_and_bands_1_1_logic_1_1_i_logic.html#a644876679baa10b43b3720f4f186b38a", null ],
    [ "UpdateLabelById", "interface_musicians_and_bands_1_1_logic_1_1_i_logic.html#a10d08851bb689654d1492f7b63880e18", null ],
    [ "UpdateMusicianById", "interface_musicians_and_bands_1_1_logic_1_1_i_logic.html#ab9842ed6c65ee34d2f54acd381fa2683", null ],
    [ "UpdateSongById", "interface_musicians_and_bands_1_1_logic_1_1_i_logic.html#a594407efaea39df2a2d53a0f8ee10aae", null ]
];