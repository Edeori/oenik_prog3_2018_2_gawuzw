var class_musicians_and_bands_api_1_1_areas_1_1_help_page_1_1_help_page_configuration_extensions =
[
    [ "GetHelpPageApiModel", "class_musicians_and_bands_api_1_1_areas_1_1_help_page_1_1_help_page_configuration_extensions.html#a98fd19b10e0c5386af73f4d2116c17b5", null ],
    [ "GetHelpPageSampleGenerator", "class_musicians_and_bands_api_1_1_areas_1_1_help_page_1_1_help_page_configuration_extensions.html#a605543816d1999d1d71bb7d1c8725bea", null ],
    [ "GetModelDescriptionGenerator", "class_musicians_and_bands_api_1_1_areas_1_1_help_page_1_1_help_page_configuration_extensions.html#a3f641471ae7dd23b375425e74f96ad33", null ],
    [ "SetActualRequestType", "class_musicians_and_bands_api_1_1_areas_1_1_help_page_1_1_help_page_configuration_extensions.html#a3b9a008bf913770a4b41e4781d79baf5", null ],
    [ "SetActualRequestType", "class_musicians_and_bands_api_1_1_areas_1_1_help_page_1_1_help_page_configuration_extensions.html#a89c9d316e4648fe3f2594e27c22fa1ff", null ],
    [ "SetActualResponseType", "class_musicians_and_bands_api_1_1_areas_1_1_help_page_1_1_help_page_configuration_extensions.html#ae152104fe590c766eb0e9d5a8c553e6b", null ],
    [ "SetActualResponseType", "class_musicians_and_bands_api_1_1_areas_1_1_help_page_1_1_help_page_configuration_extensions.html#a7d1bbd2bcc47c60c27fdad75d85696a5", null ],
    [ "SetDocumentationProvider", "class_musicians_and_bands_api_1_1_areas_1_1_help_page_1_1_help_page_configuration_extensions.html#a7fe9a117fb6cc56b97e567ca925635d1", null ],
    [ "SetHelpPageSampleGenerator", "class_musicians_and_bands_api_1_1_areas_1_1_help_page_1_1_help_page_configuration_extensions.html#a37155c1ad6d2fc69a8cb492d086f0a1a", null ],
    [ "SetSampleForMediaType", "class_musicians_and_bands_api_1_1_areas_1_1_help_page_1_1_help_page_configuration_extensions.html#a162b494fecdb51765b8a1dd258c599b4", null ],
    [ "SetSampleForType", "class_musicians_and_bands_api_1_1_areas_1_1_help_page_1_1_help_page_configuration_extensions.html#a0e46984b9685fa3d997128b5ebc73ea2", null ],
    [ "SetSampleObjects", "class_musicians_and_bands_api_1_1_areas_1_1_help_page_1_1_help_page_configuration_extensions.html#a1307c4fdcc8e22b489d17fa7561f80c8", null ],
    [ "SetSampleRequest", "class_musicians_and_bands_api_1_1_areas_1_1_help_page_1_1_help_page_configuration_extensions.html#a83b092ffe32935dcb0621355ca0eaed4", null ],
    [ "SetSampleRequest", "class_musicians_and_bands_api_1_1_areas_1_1_help_page_1_1_help_page_configuration_extensions.html#a1e0dc3504025380b55a98203883cca46", null ],
    [ "SetSampleResponse", "class_musicians_and_bands_api_1_1_areas_1_1_help_page_1_1_help_page_configuration_extensions.html#ab6f8bef6585a601ad3a840e7b2225ae7", null ],
    [ "SetSampleResponse", "class_musicians_and_bands_api_1_1_areas_1_1_help_page_1_1_help_page_configuration_extensions.html#a6de27945f12b7731047d71ea379efdd0", null ]
];