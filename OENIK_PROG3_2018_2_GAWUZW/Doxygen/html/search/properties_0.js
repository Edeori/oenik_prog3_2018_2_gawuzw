var searchData=
[
  ['album',['Album',['../class_musicians_and_bands_1_1_dtos_1_1_song_dto.html#aae8b69a2fcf3de51d619a5ddb1825e2d',1,'MusiciansAndBands::Dtos::SongDto']]],
  ['album_5fid',['Album_ID',['../class_musicians_and_bands_1_1_dtos_1_1_album_dto.html#af55edd59b2c8ac04c953d991eafe0c8f',1,'MusiciansAndBands.Dtos.AlbumDto.Album_ID()'],['../class_musicians_and_bands_1_1_dtos_1_1_song_dto.html#a50e0b1b9d8bfcbca0bd4b9e8eaf1b0d7',1,'MusiciansAndBands.Dtos.SongDto.Album_ID()']]],
  ['album_5fname',['Album_name',['../class_musicians_and_bands_1_1_dtos_1_1_album_dto.html#a336ae87564a03a47d7c23ab5d0cb9c0f',1,'MusiciansAndBands::Dtos::AlbumDto']]]
];
