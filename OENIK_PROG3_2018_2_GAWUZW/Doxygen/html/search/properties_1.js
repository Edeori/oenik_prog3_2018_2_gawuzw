var searchData=
[
  ['band',['Band',['../class_musicians_and_bands_1_1_dtos_1_1_membership_dto.html#a58abf28beaacd46511ee286ca45ffc01',1,'MusiciansAndBands.Dtos.MembershipDto.Band()'],['../class_musicians_and_bands_1_1_dtos_1_1_song_dto.html#a22648af1ce48888c40ecd50393005d14',1,'MusiciansAndBands.Dtos.SongDto.Band()']]],
  ['band_5fid',['Band_ID',['../class_musicians_and_bands_1_1_dtos_1_1_band_dto.html#a68d11350d6b4835640762e3bcf4c302e',1,'MusiciansAndBands.Dtos.BandDto.Band_ID()'],['../class_musicians_and_bands_1_1_dtos_1_1_membership_dto.html#ae68090811d21e695883cf8e41c0cfe12',1,'MusiciansAndBands.Dtos.MembershipDto.Band_ID()'],['../class_musicians_and_bands_1_1_dtos_1_1_song_dto.html#af6bcc456e3894fcafcba417c3aa178a4',1,'MusiciansAndBands.Dtos.SongDto.Band_ID()']]],
  ['band_5fname',['Band_name',['../class_musicians_and_bands_1_1_dtos_1_1_band_dto.html#afaf59c611a75517a31199e783de90f45',1,'MusiciansAndBands::Dtos::BandDto']]],
  ['bandid',['BandId',['../class_musicians_and_bands_1_1_dtos_1_1_album_dto.html#aef9f61cd5bac246279a22b01df9288c5',1,'MusiciansAndBands::Dtos::AlbumDto']]],
  ['bandname',['BandName',['../class_musicians_and_bands_1_1_dtos_1_1_album_dto.html#a5c1716bd2a8c5c54cc7b81c232ca9d00',1,'MusiciansAndBands::Dtos::AlbumDto']]]
];
