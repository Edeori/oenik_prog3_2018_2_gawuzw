var namespace_musicians_and_bands_1_1_shared =
[
    [ "AlbumException", "class_musicians_and_bands_1_1_shared_1_1_album_exception.html", "class_musicians_and_bands_1_1_shared_1_1_album_exception" ],
    [ "BandException", "class_musicians_and_bands_1_1_shared_1_1_band_exception.html", "class_musicians_and_bands_1_1_shared_1_1_band_exception" ],
    [ "GenreException", "class_musicians_and_bands_1_1_shared_1_1_genre_exception.html", "class_musicians_and_bands_1_1_shared_1_1_genre_exception" ],
    [ "LabelException", "class_musicians_and_bands_1_1_shared_1_1_label_exception.html", "class_musicians_and_bands_1_1_shared_1_1_label_exception" ],
    [ "MembersipException", "class_musicians_and_bands_1_1_shared_1_1_membersip_exception.html", "class_musicians_and_bands_1_1_shared_1_1_membersip_exception" ],
    [ "MusicianException", "class_musicians_and_bands_1_1_shared_1_1_musician_exception.html", "class_musicians_and_bands_1_1_shared_1_1_musician_exception" ],
    [ "SongException", "class_musicians_and_bands_1_1_shared_1_1_song_exception.html", "class_musicians_and_bands_1_1_shared_1_1_song_exception" ]
];